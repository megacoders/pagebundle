<?php

namespace Megacoders\PageBundle\Model;

use Doctrine\Common\Collections\ArrayCollection;
use Megacoders\PageBundle\Controller\Module\ModuleControllerInterface;

class Module
{
    /**
     * @var string
     */
    private $id;

    /**
     * @var string
     */
    private $name;

    /**
     * @var ContentEntityDescription
     */
    private $contentEntityDescription = null;

    /**
     * @var ModuleControllerInterface
     */
    private $controller;

    /**
     * @var ArrayCollection
     */
    private $actions;

    /**
     * @var ArrayCollection
     */
    private $templates;

    /**
     * Module constructor.
     */
    public function __construct()
    {
        $this->actions = new ArrayCollection();
        $this->templates = new ArrayCollection();
    }

    /**
     * @return string
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param string $id
     * @return Module
     */
    public function setId($id)
    {
        $this->id = $id;
        return $this;
    }

    /**
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @param string $name
     * @return Module
     */
    public function setName($name)
    {
        $this->name = $name;
        return $this;
    }

    /**
     * @return ContentEntityDescription
     */
    public function getContentEntityDescription()
    {
        return $this->contentEntityDescription;
    }

    /**
     * @param ContentEntityDescription $contentEntityDescription
     * @return Module
     */
    public function setContentEntityDescription($contentEntityDescription)
    {
        $this->contentEntityDescription = $contentEntityDescription;
        return $this;
    }

    /**
     * @return ModuleControllerInterface
     */
    public function getController()
    {
        return $this->controller;
    }

    /**
     * @param ModuleControllerInterface $controller
     * @return Module
     */
    public function setController($controller)
    {
        $this->controller = $controller;
        return $this;
    }

    /**
     * @return ArrayCollection
     */
    public function getActions()
    {
        return $this->actions;
    }

    /**
     * @param ArrayCollection $actions
     * @return Module
     */
    public function setActions($actions)
    {
        $this->actions = $actions;

        foreach ($actions as $parameter) {
            $this->addAction($parameter);
        }

        return $this;
    }

    /**
     * @param ModuleAction $action
     * @return Module
     */
    public function addAction(ModuleAction $action)
    {
        if (!$this->actions->contains($action)) {
            $this->actions->add($action);
        }

        return $this;
    }

    /**
     * @param string $id
     * @return ModuleAction|null
     */
    public function getAction($id) {
        foreach ($this->actions as $action) {
            if ($action->getId() == $id) {
                return $action;
            }
        }

        return null;
    }

    /**
     * @return ArrayCollection
     */
    public function getTemplates()
    {
        return $this->templates;
    }

    /**
     * @param ArrayCollection $templates
     * @return Module
     */
    public function setTemplates($templates)
    {
        $this->templates = $templates;

        foreach ($templates as $template) {
            $this->addTemplate($template);
        }

        return $this;
    }

    /**
     * @param ModuleTemplate $template
     * @return Module
     */
    public function addTemplate(ModuleTemplate $template)
    {
        if (!$this->templates->contains($template)) {
            $this->templates->add($template);
        }

        return $this;
    }

    /**
     * @param string $id
     * @return ModuleTemplate|null
     */
    public function getTemplate($id) {
        foreach ($this->templates as $template) {
            if ($template->getId() == $id) {
                return $template;
            }
        }

        return null;
    }

    /**
     * @return string
     */
    public function __toString()
    {
        return (string) $this->getName();
    }

}
