<?php

namespace Megacoders\PageBundle\Model;


use Doctrine\Common\Collections\ArrayCollection;
use Megacoders\PageBundle\Entity\Meta;
use Megacoders\PageBundle\Entity\Page;
use Megacoders\PageBundle\Entity\Site;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

class LoadedPage
{
    /**
     * @var Page
     */
    private $page;

    /**
     * @var int[]
     */
    private $parentIds = [];

    /**
     * @var Layout
     */
    private $layout;

    /**
     * @var ArrayCollection|LoadedBlock[]
     */
    private $blocks;

    /**
     * @var bool
     */
    private $debug;

    /**
     * @var Response
     */
    private $response = null;

    /**
     * @var array
     */
    private $extra = [];

    /**
     * LoadedPage constructor.
     * @param Page $page
     * @param Layout $layout
     * @param ArrayCollection $blocks
     * @param bool $debug
     */
    public function __construct(Page $page, Layout $layout, ArrayCollection $blocks, $debug = true)
    {
        $this->page = $page;
        $this->layout = $layout;
        $this->blocks = $blocks;
        $this->debug = $debug;
    }

    /**
     * @return int
     */
    public function getId()
    {
        return $this->page->getId();
    }

    /**
     * @return Page
     */
    public function getPageParent()
    {
        return $this->getPage()->getParent();
    }

    /**
     * @return int[]
     */
    public function getParentIds()
    {
        return $this->parentIds;
    }

    /**
     * @return Site
     */
    public function getSite()
    {
        return $this->page->getSite();
    }

    /**
     * @return int
     */
    public function getSiteId()
    {
        return $this->getSite()->getId();
    }

    /**
     * @return Page
     */
    public function getPage()
    {
        return $this->page;
    }

    /**
     * @return int
     */
    public function getPageId()
    {
        return $this->page->getId();
    }

    /**
     * @return string
     */
    public function getName()
    {
        return $this->getPage()->getName();
    }

    /**
     * @return string
     */
    public function getTitle() {
        $metaTitle = $this->getMeta('title');

        if ($metaTitle) {
            return $metaTitle->getContent();
        }

        return $this->getName();
    }

    /**
     * @param bool $absolute
     * @return string
     */
    public function getUrl($absolute = false)
    {
        return $this->getPage()->getUrl($absolute);
    }

    /**
     * @return ArrayCollection|Meta[]
     */
    public function getMetas()
    {
        return $this->getPage()->getMetas()->filter(function(Meta $meta) {
            $name = $meta->getName();
            return $name != 'title' && $name[0] != '-';
        });
    }

    /**
     * @return ArrayCollection|Meta[]
     */
    public function getHiddenMetas()
    {
        return $this->getPage()->getMetas()->filter(function(Meta $meta) {
            $name = $meta->getName();
            return $name[0] == '-';
        });
    }

    /**
     * @param string $name
     * @return Meta|null
     */
    public function getMeta($name) {
        return $this->getPage()->getMeta($name);
    }

    /**
     * @param Meta $meta
     * @return LoadedPage
     */
    public function addMeta(Meta $meta)
    {
        $this->getPage()->addMeta($meta);
        return $this;
    }

    /**
     * @param Meta $meta
     * @return LoadedPage
     */
    public function setMeta(Meta $meta)
    {
        $this->getPage()->setMeta($meta);
        return $this;
    }

    /**
     * @param string $areaId
     * @return LoadedBlock|null
     */
    public function getBlock($areaId)
    {
        return $this->blocks->get($areaId);
    }

    /**
     * @return Layout
     */
    public function getLayout()
    {
        return $this->layout;
    }

    /**
     * @return string
     */
    public function getTemplate()
    {
        return $this->getLayout()->getTemplate();
    }

    /**
     * @return Response
     */
    public function getResponse()
    {
        return $this->response;
    }

    /**
     * @param string $name
     * @param mixed $default
     * @return mixed
     */
    public function getExtra($name, $default = null)
    {
        return isset($this->extra[$name]) ? $this->extra[$name] : $default;
    }

    /**
     * @param string $name
     * @param mixed $value
     * @return LoadedPage
     */
    public function setExtra($name, $value)
    {
        $this->extra[$name] = $value;
        return $this;
    }

    /**
     * @param Request $request
     * @return Response
     */
    public function handle(Request $request)
    {
        $request->attributes->set('_page', $this);
        $this->parentIds = $request->get('_pageParentIds');

        $mainArea = $this->layout->getMainArea();

        if ($mainArea !== null) {
            $mainBlock = $this->getBlock($mainArea->getId());

            if ($mainBlock !== null) {
                $this->response = $mainBlock->handle($request);
            }
        }

        if (!$request->isXmlHttpRequest()) {
            foreach ($this->blocks as $block) {
                if ($block->isMain()) {
                    continue;
                }

                $block->handle($request);
            }
        }

        return $this->response;
    }
}
