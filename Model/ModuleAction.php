<?php

namespace Megacoders\PageBundle\Model;

use Doctrine\Common\Collections\ArrayCollection;

class ModuleAction
{

    const DEFAULT_ACTION_ID = 'index';

    /**
     * @var string
     */
    private $id;

    /**
     * @var string
     */
    private $name;

    /**
     * @var ArrayCollection|ModuleParameter[]
     */
    private $parameters;

    /**
     * ModuleAction constructor.
     * @param string $id
     * @param string $name
     */
    public function __construct($id, $name)
    {
        $this->id = $id;
        $this->name = $name;
        $this->parameters = new ArrayCollection();
    }

    /**
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @return ArrayCollection|ModuleParameter[]
     */
    public function getParameters()
    {
        return $this->parameters;
    }

    /**
     * @param ModuleParameter $parameter
     * @return ModuleAction
     */
    public function addParameter(ModuleParameter $parameter)
    {
        if (!$this->parameters->contains($parameter)) {
            $this->parameters->add($parameter);
        }

        return $this;
    }

    /**
     * @return string
     */
    public function __toString()
    {
        return (string) $this->getName();
    }
}
