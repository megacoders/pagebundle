<?php

namespace Megacoders\PageBundle\Model;

class ContentEntityDescription
{

    /**
     * @var string
     */
    private $class = null;

    /**
     * @var string
     */
    private $idParameter = null;

    /**
     * @var string
     */
    private $route = null;

    /**
     * @var array
     */
    private $filter = [];

    /**
     * ModuleContentEntity constructor.
     * @param array $properties
     */
    public function __construct(array $properties = [])
    {
        if (isset($properties['class'])) {
            $this->class = $properties['class'];
        }

        if (isset($properties['id_parameter'])) {
            $this->idParameter = $properties['id_parameter'];
        }

        if (isset($properties['route'])) {
            $this->route = $properties['route'];
        }

        if (isset($properties['filter'])) {
            $this->filter = $properties['filter'];
        }
    }

    /**
     * @return string
     */
    public function getClass()
    {
        return $this->class;
    }

    /**
     * @return string
     */
    public function getIdParameter()
    {
        return $this->idParameter;
    }

    /**
     * @return string
     */
    public function getRoute()
    {
        return $this->route;
    }

    /**
     * @return array
     */
    public function getFilter()
    {
        return $this->filter;
    }

}
