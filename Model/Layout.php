<?php

namespace Megacoders\PageBundle\Model;

use Doctrine\Common\Collections\ArrayCollection;

class Layout
{
    /**
     * @var string
     */
    private $id;

    /**
     * @var string
     */
    private $name;

    /**
     * @var bool
     */
    private $hidden;

    /**
     * @var string
     */
    private $template;

    /**
     * @var ArrayCollection
     */
    private $areas;

    /**
     * @var LayoutArea
     */
    private $mainArea = null;

    /**
     * Layout constructor.
     */
    public function __construct()
    {
        $this->areas = new ArrayCollection();
    }

    /**
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param int $id
     * @return Layout
     */
    public function setId($id)
    {
        $this->id = $id;
        return $this;
    }

    /**
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @param string $name
     * @return Layout
     */
    public function setName($name)
    {
        $this->name = $name;
        return $this;
    }

    /**
     * @return boolean
     */
    public function isHidden()
    {
        return $this->hidden;
    }

    /**
     * @param boolean $hidden
     * @return Layout
     */
    public function setHidden($hidden)
    {
        $this->hidden = $hidden;
        return $this;
    }

    /**
     * @return string
     */
    public function getTemplate()
    {
        return $this->template;
    }

    /**
     * @param string $template
     * @return Layout
     */
    public function setTemplate($template)
    {
        $this->template = $template;
        return $this;
    }

    /**
     * @param bool $mainFirst
     * @return ArrayCollection|LayoutArea[]
     */
    public function getAreas($mainFirst = true)
    {
        $areas = $this->areas->getValues();

        if (!$mainFirst) {
            return new ArrayCollection($areas);
        }

        uasort($areas, function(LayoutArea $a, LayoutArea $b) {
            return $a->isMain() === $b->isMain()
                ? 0
                : ($a->isMain() > $b->isMain() ? -1 : 1);
        });

        return new ArrayCollection($areas);
    }

    /**
     * @param ArrayCollection $areas
     * @return Layout
     */
    public function setAreas($areas)
    {
        $this->areas->clear();

        foreach ($areas as $area) {
            $this->addArea($area);
        }

        return $this;
    }

    /**
     * @param LayoutArea $area
     * @return Layout
     */
    public function addArea(LayoutArea $area)
    {
        if (!$this->areas->contains($area)) {
            $this->areas->add($area);

            if ($area->isMain()) {
                $this->mainArea = $area;
            }
        }

        return $this;
    }

    /**
     * @return LayoutArea
     */
    public function getMainArea()
    {
        return $this->mainArea;
    }

    /**
     * @param LayoutArea $mainArea
     * @return Layout
     */
    public function setMainArea($mainArea)
    {
        $this->mainArea = $mainArea;
        return $this;
    }

    /**
     * @param string $id
     * @return LayoutArea|null
     */
    public function getArea($id) {
        foreach ($this->areas as $area) {
            if ($area->getId() == $id) {
                return $area;
            }
        }

        return null;
    }

    /**
     * @return string
     */
    public function __toString()
    {
        return (string) $this->getName();
    }
}
