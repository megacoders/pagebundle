<?php

namespace Megacoders\PageBundle\Model;

use Ivory\CKEditorBundle\Form\Type\CKEditorType;
use Megacoders\AdminBundle\Form\Type\EntityChoiceType;
use Megacoders\PageBundle\Form\Type\PageChoiceType;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\Extension\Core\Type\TextType;

class ModuleParameter
{
    const DEFAULT_TYPE_CLASS = TextType::class;

    const TYPE_MAPPING = [
        'richText' => CKEditorType::class,
        'entity' => EntityType::class,
        'entity_choice' => EntityChoiceType::class,
        'page_choice' => PageChoiceType::class
    ];

    /**
     * @param string $type
     * @return string
     */
    public static function getTypeClass($type)
    {
        $types = self::TYPE_MAPPING;

        return isset($types[$type]) ? $types[$type] : $type;
    }

    /**
     * @var string
     */
    private $id;

    /**
     * @var string
     */
    private $name;

    /**
     * @var string
     */
    private $type;

    /**
     * @var array
     */
    private $options = [];

    /**
     * ModuleParameter constructor.
     * @param string $id
     * @param string $name
     * @param string $type
     * @param array $options
     */
    public function __construct($id, $name, $type, $options)
    {
        $this->id = $id;
        $this->name = $name;
        $this->type = $type;
        $this->options = $options;
    }

    /**
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @return string
     */
    public function getType()
    {
        return $this->type;
    }

    /**
     * @return array
     */
    public function getOptions()
    {
        return array_merge(['label' => $this->getName()], $this->options);
    }

    /**
     * @return string
     */
    public function __toString()
    {
        return (string) $this->getName();
    }
}
