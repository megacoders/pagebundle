<?php

namespace Megacoders\PageBundle\Model;

use Megacoders\PageBundle\Http\ErrorResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

class LoadedBlock
{
    /**
     * @var LayoutArea
     */
    private $area;

    /**
     * @var Module
     */
    private $module;

    /**
     * @var string
     */
    private $actionId;

    /**
     * @var string
     */
    private $templateId;

    /**
     * @var string
     */
    private $title;

    /**
     * @var array
     */
    private $parameters;

    /**
     * @var bool
     */
    private $debug;

    /**
     * @var Response
     */
    private $response = null;

    /**
     * LoadedBlock constructor.
     * @param LayoutArea $area
     * @param Module $module
     * @param string $actionId
     * @param string $templateId
     * @param array|null $parameters
     * @param bool $debug
     */
    public function __construct(LayoutArea $area, Module $module, $actionId, $templateId, $title, $parameters = [], $debug = true)
    {
        $this->area = $area;
        $this->module = $module;
        $this->actionId = $actionId;
        $this->templateId = $templateId;
        $this->title = $title;
        $this->parameters = $parameters !== null ? $parameters: [];
        $this->debug = $debug;
    }

    /**
     * @return LayoutArea
     */
    public function getArea()
    {
        return $this->area;
    }

    /**
     * @return Module
     */
    public function getModule()
    {
        return $this->module;
    }

    /**
     * @return string
     */
    public function getActionId()
    {
        return $this->actionId;
    }

    /**
     * @return string
     */
    public function getTemplateId() {
        return $this->templateId;
    }

    /**
     * @return string
     */
    public function getTitle()
    {
        return $this->title;
    }

    /**
     * @return array
     */
    public function getParameters()
    {
        return $this->parameters;
    }

    /**
     * @return bool
     */
    public function isMain()
    {
        return $this->getArea()->isMain();
    }

    /**
     * @return Response
     */
    public function getResponse()
    {
        return $this->response;
    }

    /**
     * @param Request $request
     * @return Response
     */
    public function handle(Request $request)
    {
        try {
            $path = $request->attributes->all();
            $path['_area'] = $this->getArea();
            $path['_template'] = $this->getTemplateId();
            $path['_block_title'] = $this->getTitle();
            $path['_parameters'] = $this->getParameters();

            if (!$this->getArea()->isMain() || empty($path['_action'])) {
                $path['_action'] = $this->getActionId();
            }

            $subRequest = $request->duplicate($request->query->all(), $request->request->all(), $path);
            $this->response = $this->getModule()->getController()->handle($subRequest);

        } catch (\Throwable $exception) {
            $this->response = new ErrorResponse($exception);
        }

        return $this->response;
    }

    /**
     * @return string
     */
    public function __toString()
    {
        return $this->response !== null ? $this->response->getContent() : '';
    }
}
