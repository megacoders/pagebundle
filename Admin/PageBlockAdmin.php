<?php

namespace Megacoders\PageBundle\Admin;

use Megacoders\AdminBundle\Admin\BaseAdmin;
use Megacoders\PageBundle\Entity\Block;
use Megacoders\PageBundle\Entity\PageBlock;
use Megacoders\PageBundle\Form\DataTransformer\StringToLayoutAreaTransformer;
use Megacoders\PageBundle\Form\DataTransformer\StringToModuleTransformer;
use Megacoders\PageBundle\Manager\LayoutManager;
use Megacoders\PageBundle\Manager\ModuleManager;
use Megacoders\PageBundle\Model\Layout;
use Megacoders\PageBundle\Model\LayoutArea;
use Sonata\AdminBundle\Datagrid\DatagridMapper;
use Sonata\AdminBundle\Datagrid\ListMapper;
use Sonata\AdminBundle\Form\FormMapper;
use Sonata\AdminBundle\Route\RouteCollection;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\FormEvent;
use Symfony\Component\Form\FormEvents;

class PageBlockAdmin extends BaseAdmin
{
    /**
     * @var string
     */
    protected $baseRoutePattern = 'blocks';

    /**
     * @var array
     */
    protected $datagridValues = array(
        '_page' => 1,
        '_sort_order' => 'ASC',
        '_sort_by' => 'name',
    );

    /**
     * @var string
     */
    protected $parentAssociationMapping = 'page';

    /**
     * {@inheritdoc}
     */
    protected $accessMapping = array(
        'grid' => 'LIST'
    );

    /**
     * {@inheritdoc}
     */
    protected function configureRoutes(RouteCollection $collection)
    {
        parent::configureRoutes($collection);

        $collection->add('grid', 'grid');
        $collection->add('module_blocks', 'module-blocks');
        $collection->add('module_templates', 'module-templates');
    }

    /**
     * {@inheritdoc}
     */
    protected function configureDatagridFilters(DatagridMapper $datagridMapper)
    {
        $modules = $this->getModuleManager()->getAll();
        $modulesChoices = [];

        foreach ($modules as $module) {
            $modulesChoices[$module->getName()] = $module->getId();
        }

        $datagridMapper
            ->add('block.moduleId', null, ['label' => 'admin.entities.block.module'], ChoiceType::class, [
                'choices' => $modulesChoices
            ])
        ;
    }

    /**
     * {@inheritdoc}
     */
    protected function configureListFields(ListMapper $listMapper)
    {
        $listMapper
            ->add('areaId', null, [
                'label' => 'admin.entities.page_block.area',
                'template' => 'MegacodersPageBundle:Admin/PageBlock:list__field__area.html.twig'
            ])
            ->add('block', null, ['label' => 'admin.entities.page_block.block'])
            ->add('_action', null, [
                'label' => 'admin.actions._actions',
                'actions' => ['edit' => [], 'delete' => []]
            ])
        ;
    }

    /**
     * {@inheritdoc}
     */
    protected function configureFormFields(FormMapper $formMapper)
    {
        /** @var ModuleManager $moduleManager */
        $moduleManager = $this->getConfigurationPool()->getContainer()->get('page.manager.module_manager');

        /** @var PageBlock $object */
        $object = $this->getSubject();
        $objectModuleId = $object ? $object->getModuleId() : null;

        $layout = $this->getPageLayout($this->getSubject());

        $modelManager = $this->getModelManager();

        $formMapper->getFormBuilder()->addEventListener(
            FormEvents::PRE_SUBMIT,
            function(FormEvent $event) use ($modelManager) {
                $form = $event->getForm();
                $data = $event->getData();

                if (isset($data['block'])) {
                    $form->remove('block')->add('block', null, ['choices' => [
                        $data['block'] => $modelManager->find(Block::class, $data['block'])
                    ]]);
                }

                if (isset($data['templateId'])) {
                    $form->remove('templateId')->add('templateId', ChoiceType::class, ['choices' => [
                        $data['templateId'] => $data['templateId']
                    ]]);
                }
            }
        );

        $areaId = $this->getRequest()->get('areaId');

        $formMapper
            ->with('admin.labels.page_block')
                ->add('areaId', ChoiceType::class, [
                    'label' => 'admin.entities.page_block.area',
                    'data' => $areaId,
                    'choices' => $layout->getAreas(),
                    'choice_label' => 'name',
                    'choice_value' => 'id'
                ])
                ->add('moduleId', ChoiceType::class, [
                    'label' => 'admin.entities.page_block.module',
                    'mapped' => false,
                    'data' => $objectModuleId,
                    'choices' => $moduleManager->getAll(),
                    'choice_label' => 'name',
                    'choice_value' => 'id'
                ])
                ->add('block', null, [
                    'label' => 'admin.entities.page_block.block',
                    'disabled' => true
                ])
                ->add('templateId', ChoiceType::class, [
                    'label' => 'admin.entities.block.template',
                    'attr' => ['disabled' => true]
                ])
            ->end()
        ;

        $formMapper->get('moduleId')->addModelTransformer(new StringToModuleTransformer($moduleManager));
        $formMapper->get('areaId')->addModelTransformer(new StringToLayoutAreaTransformer($layout));
    }

    /**
     * @param PageBlock $pageBlock
     * @return Layout|null
     */
    protected function getPageLayout(PageBlock $pageBlock)
    {
        $page = $pageBlock->getPage();

        /** @var LayoutManager $layoutManager */
        $layoutManager = $this->getConfigurationPool()->getContainer()->get('page.manager.layout_manager');

        return $layoutManager->get($page->getLayoutId());
    }

    /**
     * @return ModuleManager
     */
    protected function getModuleManager()
    {
        /** @var ModuleManager $moduleManager */
        $moduleManager = $this->getConfigurationPool()->getContainer()->get('page.manager.module_manager');

        return $moduleManager;
    }

    /**
     * @param PageBlock $pageBlock
     * @param string $areaId
     * @return LayoutArea|null
     */
    public function getArea(PageBlock $pageBlock, $areaId)
    {
        $layout = $this->getPageLayout($pageBlock);

        if ($layout === null) {
            return null;
        }

        return $layout->getArea($areaId);
    }
}
