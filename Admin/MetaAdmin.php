<?php

namespace Megacoders\PageBundle\Admin;

use Megacoders\AdminBundle\Admin\BaseAdmin;
use Sonata\AdminBundle\Datagrid\ListMapper;
use Sonata\AdminBundle\Form\FormMapper;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;

class MetaAdmin extends BaseAdmin
{
    /**
     * @var string
     */
    protected $baseRoutePattern = 'metas';

    /**
     * @var array
     */
    protected $datagridValues = array(
        '_page' => 1,
        '_sort_order' => 'ASC',
        '_sort_by' => 'name',
    );

    /**
     * @var string
     */
    protected $parentAssociationMapping = 'page';

    /**
     * {@inheritdoc}
     */
    protected function configureListFields(ListMapper $listMapper)
    {
        $listMapper
            ->add('name', null, ['label' => 'admin.entities.meta.name'])
            ->add('content', null, ['label' => 'admin.entities.meta.content'])
            ->add('_action', null, [
                'label' => 'admin.actions._actions',
                'actions' => ['edit' => [], 'delete' => []]
            ])
        ;
    }

    /**
     * {@inheritdoc}
     */
    protected function configureFormFields(FormMapper $formMapper)
    {
        $formMapper
            ->with('admin.labels.meta')
                ->add('name', null, ['label' => 'admin.entities.meta.name'])
                ->add('content', null, ['label' => 'admin.entities.meta.content'])
                ->add('property', ChoiceType::class, [
                    'label' => 'admin.entities.meta.property',
                    'choices' => $this->getMetaPropertiesChoices()
                ])
            ->end()
        ;
    }

    /**
     * @return array
     */
    protected function getMetaPropertiesChoices()
    {
        return [
            'property' => 'property',
            'name' => 'name'
        ];
    }
}
