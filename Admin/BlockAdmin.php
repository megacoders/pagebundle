<?php

namespace Megacoders\PageBundle\Admin;

use Doctrine\ORM\Query\Expr;
use Doctrine\ORM\QueryBuilder;
use Megacoders\AdminBundle\Admin\BaseAdmin;
use Megacoders\AdminBundle\Form\DataTransformer\IdArrayToCollectionTransformer;
use Megacoders\AdminBundle\Form\DataTransformer\IdToModelTransformer;
use Megacoders\PageBundle\Entity\Block;
use Megacoders\PageBundle\Entity\Page;
use Megacoders\PageBundle\Form\DataTransformer\StringToModuleTransformer;
use Megacoders\PageBundle\Form\Type\PageChoiceType;
use Megacoders\PageBundle\Manager\ModuleManager;
use Megacoders\PageBundle\Model\Module;
use Megacoders\PageBundle\Model\ModuleAction;
use Megacoders\PageBundle\Model\ModuleParameter;
use Sonata\AdminBundle\Datagrid\DatagridMapper;
use Sonata\AdminBundle\Datagrid\ListMapper;
use Sonata\AdminBundle\Form\FormMapper;
use Sonata\AdminBundle\Route\RouteCollection;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\FormEvent;
use Symfony\Component\Form\FormEvents;
use Symfony\Component\Serializer\NameConverter\CamelCaseToSnakeCaseNameConverter;

class BlockAdmin extends BaseAdmin
{
    /**
     * @var string
     */
    protected $baseRoutePattern = 'structure/blocks';

    /**
     * @var string
     */
    protected $parametersFieldPrefix = '_p_';

    /**
     * {@inheritdoc}
     */
    protected function configureRoutes(RouteCollection $collection)
    {
        parent::configureRoutes($collection);

        $collection->add('module_actions', 'module-actions');
        $collection->add('module_action_parameters', 'module-action-parameters');
    }

    /**
     * {@inheritdoc}
     */
    protected function configureDatagridFilters(DatagridMapper $datagridMapper)
    {
        $modules = $this->getModuleManager()->getAll();
        $modulesChoices = [];

        foreach ($modules as $module) {
            $modulesChoices[$module->getName()] = $module->getId();
        }

        $datagridMapper
            ->add('name', null, ['label' => 'admin.entities.block.name'])
            ->add('moduleId', null, ['label' => 'admin.entities.block.module'], ChoiceType::class, [
                'choices' => $modulesChoices
            ])
        ;
    }

    /**
     * {@inheritdoc}
     */
    protected function configureListFields(ListMapper $listMapper)
    {
        $listMapper
            ->add('name', null, ['label' => 'admin.entities.block.name'])
            ->add('moduleId', null, [
                'label' => 'admin.entities.block.module',
                'template' => 'MegacodersPageBundle:Admin/Block:list__field__module.html.twig'
            ])
            ->add('actionId', null, [
                'label' => 'admin.entities.block.action',
                'template' => 'MegacodersPageBundle:Admin/Block:list__field__action.html.twig'
            ])
            ->add('_action', null, [
                'label' => 'admin.actions._actions',
                'actions' => ['edit' => [], 'delete' => []]
            ])
        ;
    }

    /**
     * {@inheritdoc}
     */
    protected function configureFormFields(FormMapper $formMapper)
    {
        if ('parameters' == $this->getRequest()->get('_form')) {
            $this->configureModuleParametersFields($formMapper);
            return;
        }

        $formMapper->getFormBuilder()->addEventListener(FormEvents::PRE_SUBMIT, function(FormEvent $event) {
            $form = $event->getForm();
            $data = $event->getData();

            $checkboxParams = [];

            if (isset($data['moduleId']) && isset($data['actionId'])) {
                $moduleManager = $this->getModuleManager();
                $moduleParameters = $moduleManager->getModuleParameters($data['moduleId'], $data['actionId']);

                if ($moduleParameters != null) {
                    foreach ($moduleParameters as $parameter) {
                        if ('checkbox' == $parameter->getType()) {
                            $checkboxParams[$parameter->getId()] = true;
                        }
                    }
                }
            }

            if (isset($data['actionId'])) {
                $form->remove('actionId')->add('actionId', ChoiceType::class, ['choices' => [
                    $data['actionId'] => $data['actionId']
                ]]);
            }

            foreach ($data as $key => $value) {
                $prefixLength = strlen($this->parametersFieldPrefix);

                if (substr($key, 0, $prefixLength) === $this->parametersFieldPrefix) {
                    $id = substr($key, $prefixLength);
                    unset($checkboxParams[$id]);

                    $form->remove($key)->add($key, null, [
                        'data' => $value,
                        'property_path' => 'parameters[' .$id .']'
                    ]);
                }
            }

            foreach (array_keys($checkboxParams) as $id) {
                $key = $this->parametersFieldPrefix .$id;

                $form->add($key, null, [
                    'data' => false,
                    'property_path' => 'parameters[' .$id .']'
                ]);
            }
        });

        $moduleManager = $this->getModuleManager();

        $formMapper
            ->with('admin.labels.block')
                ->add('name', null, ['label' => 'admin.entities.block.name'])
                ->add('moduleId', ChoiceType::class, [
                    'label' => 'admin.entities.block.module',
                    'choices' => $moduleManager->getAll(),
                    'choice_label' => 'name',
                    'choice_value' => 'id'
                ])
                ->add('actionId', ChoiceType::class, [
                    'label' => 'admin.entities.block.action',
                    'attr' => ['disabled' => true]
                ])
                ->add('title', null, ['label' => 'admin.entities.block.title'])
            ->end()
            ->with('admin.labels.block_parameters', ['box_class' => 'box box-primary box-block-parameters hidden'])->end()
        ;

        $formMapper->get('moduleId')->addModelTransformer(new StringToModuleTransformer($moduleManager));
    }

    protected function configureModuleParametersFields(FormMapper $formMapper)
    {
        $formMapper->with('admin.labels.block_parameters', ['box_class' => 'box box-primary box-block-parameters']);

        /** @var Block $block */
        $block = $this->getSubject();
        $moduleId = $block ? $block->getModuleId() : null;
        $actionId = $block ? $block->getActionId() : null;
        $blockParameters = [];

        if ($moduleId && $actionId) {
            $moduleManager = $this->getModuleManager();
            $module = $moduleManager->get($moduleId);

            if ($module === null) {
                return;
            }

            $action = $module->getAction($actionId);

            if ($action === null) {
                return;
            }

            $parameters = $action->getParameters();
            $values = $block ? $block->getParameters() : [];

            if ($parameters->count()) {
                /** @var ModuleParameter $parameter */
                foreach ($parameters as $parameter) {
                    $id = $parameter->getId();
                    $type = $parameter->getType();
                    $options = $parameter->getOptions();

                    if (isset($options['constraints'])) {
                        $options['constraints'] = $this->getConstraintsForModuleParameter($options['constraints']);
                    }

                    if (isset($options['default'])) {
                        if (!isset($values[$id])) {
                            $options['data'] = $options['default'];
                        }

                        unset($options['default']);
                    }

                    if ($type == 'checkbox' && isset($values[$id])) {
                        $values[$id] = (bool) $values[$id];
                    }

                    $blockParameters[$id] = isset($values[$id])
                        ? $values[$id]
                        : (isset($options['data']) ? $options['data'] : null)
                    ;

                    if ($type == EntityType::class && isset($options['class'])) {
                        $queryOptions = isset($options['query']) ? $options['query'] : [];
                        unset($options['query']);

                        $options['query_builder'] = $this->getQueryForEntityModuleParameter($options['class'], $queryOptions);
                    }

                    $options['property_path'] = 'parameters[' .$id .']';
                    $fieldName = $this->parametersFieldPrefix .$id;
                    $formMapper->add($fieldName, $type, $options);

                    if (in_array($type, [EntityType::class, PageChoiceType::class])) {
                        $modelManager = $this->getModelManager();
                        $className = $type == PageChoiceType::class ? Page::class : $options['class'];

                        if (isset($options['multiple']) && $options['multiple']) {
                            $formMapper->get($fieldName)->addModelTransformer(new IdArrayToCollectionTransformer(
                                $modelManager, $className
                            ));

                        } else {
                            $formMapper->get($fieldName)->addModelTransformer(new IdToModelTransformer(
                                $modelManager, $className
                            ));
                        }
                    }
                }
            }
        }

        if ($block) {
            $block->setParameters($blockParameters);
        }

        $formMapper->end();
    }

    /**
     * @param string $class
     * @param array $options
     * @return QueryBuilder
     */
    protected function getQueryForEntityModuleParameter($class, $options)
    {
        $wheres = isset($options['criteria']) ? $options['criteria'] : [];
        $orderBy = isset($options['orderBy']) ? $options['orderBy'] : null;
        $limit = isset($options['limit']) ? $options['limit'] : null;
        $offset = isset($options['offset']) ? $options['offset'] : null;

        $expr = new Expr();
        $queryBuilder = $this->getEntityRepository($class)->createQueryBuilder('o');

        if (count($wheres)) {
            foreach ($wheres as $name => $value) {
                if (is_array($value) && count($value)) {
                    $method = $value[0];
                    $value = isset($value[1]) ? $value[1] : null;
                    $whereMethod = isset($value[2]) ? strtolower($value[2]) : 'and';

                    if (!in_array($whereMethod, ['and', 'or'])) {
                        $whereMethod = 'and';
                    }

                    if (method_exists($expr, $method)) {
                        $queryBuilder->{$whereMethod .'Where'}($expr->{$method}('o.' .$name, $expr->literal($value)));
                    }

                } else {
                    $queryBuilder->andWhere($expr->eq('o.' .$name, $expr->literal($value)));
                }
            }
        }

        if ($orderBy) {
            $queryBuilder->orderBy($orderBy);
        }

        $queryBuilder->setFirstResult($offset);
        $queryBuilder->setMaxResults($limit);

        return $queryBuilder;
    }

    /**
     * @param array $constraintInfos
     * @return array
     */
    protected function getConstraintsForModuleParameter($constraintInfos)
    {
        $constraints = [];

        foreach ($constraintInfos as $info) {
            if (isset($info['class'])) {
                $className = $info['class'];

                if (!class_exists($className)) {
                    $converter = new CamelCaseToSnakeCaseNameConverter(null, false);
                    $className = 'Symfony\\Component\\Validator\\Constraints\\' .$converter->denormalize($className);

                    if (!class_exists($className)) {
                        continue;
                    }
                }

                $constraints[] = new $className(isset($info['arguments']) ? $info['arguments'] : null);
            }
        }

        return $constraints;
    }

    /**
     * @return ModuleManager
     */
    protected function getModuleManager()
    {
        /** @var ModuleManager $moduleManager */
        $moduleManager = $this->getConfigurationPool()->getContainer()->get('page.manager.module_manager');

        return $moduleManager;
    }

    /**
     * @param string $moduleId
     * @return Module|null
     */
    public function getModule($moduleId)
    {
        return $this->getModuleManager()->get($moduleId);
    }

    /**
     * @param string $moduleId
     * @param string $actionId
     * @return ModuleAction|null
     */
    public function getModuleAction($moduleId, $actionId)
    {
        $module = $this->getModule($moduleId);

        return $module === null ? null : $module->getAction($actionId);
    }
}
