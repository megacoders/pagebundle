<?php

namespace Megacoders\PageBundle\Admin;

use Doctrine\ORM\Query\Expr;
use Megacoders\AdminBundle\Admin\BaseAdmin;
use Megacoders\PageBundle\Entity\Site;
use Megacoders\PageBundle\Form\Type\PageChoiceType;
use Sonata\AdminBundle\Datagrid\ListMapper;
use Sonata\AdminBundle\Form\FormMapper;
use Symfony\Component\Form\Extension\Core\Type\UrlType;

class SiteAdmin extends BaseAdmin
{
    /**
     * @var string
     */
    protected $baseRoutePattern = 'structure/sites';

    /**
     * @var array
     */
    protected $datagridValues = array(
        '_page' => 1,
        '_sort_order' => 'DESC',
        '_sort_by' => 'main',
    );

    /**
     * {@inheritdoc}
     */
    protected function configureListFields(ListMapper $listMapper)
    {
        $listMapper
            ->add('name', null, ['label' => 'admin.entities.site.name'])
            ->add('host', null, ['label' => 'admin.entities.site.host'])
            ->add('mainPage', null, ['label' => 'admin.entities.site.main_page'])
            ->add('main', null, ['label' => 'admin.entities.site.main'])
            ->add('_action', null, [
                'label' => 'admin.actions._actions',
                'actions' => ['edit' => [], 'delete' => []]
            ])
        ;
    }

    /**
     * {@inheritdoc}
     */
    protected function configureFormFields(FormMapper $formMapper)
    {
        $mainEditOnly = $this->isEditMode() && $this->getSubject()->isMain();

        /** @var Site $site */
        $site = $this->getSubject();

        $formMapper->with('admin.labels.site')
            ->add('name', null, ['label' => 'admin.entities.site.name'])
            ->add('host', UrlType::class, ['label' => 'admin.entities.site.host'])
        ;

        if ($site) {
            $formMapper->add('mainPage', PageChoiceType::class, [
                'label' => 'admin.entities.site.main_page',
                'required' => false,
                'placeholder' => '— Auto —',
                'site_id' => $site->getId()
            ]);
        }

        $formMapper
            ->add('main', null, [
                'label' => 'admin.entities.site.main',
                'mapped' => !$mainEditOnly,
                'attr' => [
                    'disabled' => $mainEditOnly,
                    'checked' => $this->getSubject()->isMain()
                ]
            ])
            ->end()
        ;
    }

    public function postUpdate($object)
    {
        $this->updateMainSite($object);
    }

    public function postPersist($object)
    {
        $this->updateMainSite($object);
    }

    protected function updateMainSite(Site $object)
    {
        if ($object->isMain()) {
            $expr = new Expr();

            $this->getEntityRepository(Site::class)
                ->createQueryBuilder('s')
                    ->update()
                        ->set('s.main', $expr->literal(false))
                    ->where($expr->neq('s.id', $object->getId()))
                ->getQuery()
                    ->execute()
            ;
        }
    }
}
