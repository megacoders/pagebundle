<?php

namespace Megacoders\PageBundle\Admin;

use Doctrine\ORM\EntityManager;
use Megacoders\AdminBundle\Admin\BaseAdmin;
use Megacoders\PageBundle\Entity\Page;
use Megacoders\PageBundle\Entity\Site;
use Megacoders\PageBundle\Form\DataTransformer\StringToLayoutTransformer;
use Megacoders\PageBundle\Form\Type\PageChoiceType;
use Megacoders\PageBundle\Manager\LayoutManager;
use Megacoders\PageBundle\Manager\SiteManager;
use Knp\Menu\ItemInterface as MenuItemInterface;
use Sonata\AdminBundle\Admin\AdminInterface;
use Sonata\AdminBundle\Datagrid\ListMapper;
use Sonata\AdminBundle\Form\FormMapper;
use Sonata\AdminBundle\Route\RouteCollection;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\HttpFoundation\Session\Session;

class PageAdmin extends BaseAdmin
{
    /**
     * @var string
     */
    protected $baseRoutePattern = 'structure/pages';

    /**
     * @var array
     */
    protected $datagridValues = array(
        '_page' => 1,
        '_sort_order' => 'ASC',
        '_sort_by' => 'sortOrder',
    );

    /**
     * {@inheritdoc}
     */
    protected $accessMapping = array(
        'tree' => 'LIST',
        'blocks' => 'EDIT'
    );

    /**
     * {@inheritdoc}
     */
    public function configureRoutes(RouteCollection $collection)
    {
        parent::configureRoutes($collection);

        $collection->add('tree', 'tree');
        $collection->add('select_site', 'select-site/{id}');
    }

    /**
     * @param string $layoutId
     * @return string|string
     */
    public function getLayoutName($layoutId)
    {
        /** @var LayoutManager $layoutManager */
        $layoutManager = $this->getConfigurationPool()->getContainer()->get('page.manager.layout_manager');
        $layout = $layoutManager->get($layoutId);

        return $layout !== null ? $layout->getName() : null;
    }

    /**
     * @param int $siteId
     */
    public function storeSiteId($siteId)
    {
        $this->getConfigurationPool()->getContainer()->get('session')->set(Site::ADMIN_SESSION_PARAMETER_NAME, $siteId);
    }

    /**
     * @return int
     */
    public function getSiteId()
    {
        /** @var Session $session */
        $session = $this->getConfigurationPool()->getContainer()->get('session');
        $siteId = $session->get(Site::ADMIN_SESSION_PARAMETER_NAME);

        if ($siteId === null) {
            /** @var SiteManager $siteManager */
            $siteManager = $this->getConfigurationPool()->getContainer()->get('page.manager.site_manager');
            $site = $siteManager->loadMain();

            if ($site !== null) {
                $siteId = $site->getId();
                $session->set(Site::ADMIN_SESSION_PARAMETER_NAME, $siteId);
            }
        }

        return $siteId;
    }

    /**
     * @param Page $page
     */
    public function preRemove($page)
    {
        $site = $page->getSite();

        if ($site->getMainPage() === $page) {
            $site->setMainPage(null);

            /** @var Session $session */
            $session = $this->getConfigurationPool()->getContainer()->get('session');
            $session->getFlashBag()->add(
                'warning',
                sprintf('"%s" site main page has been changed to — Auto —.', $site->getName())
            );
        }
    }

    /**
     * @param Page $page
     */
    public function prePersist($page)
    {
        $this->checkPageLocale($page);
        $this->checkPagePublished($page);
    }

    /**
     * @param Page $page
     */
    public function preUpdate($page)
    {
        $this->checkPageLocale($page);
        $this->checkPagePublished($page);
    }

    /**
     * {@inheritdoc}
     */
    protected function configureTabMenu(MenuItemInterface $menu, $action, AdminInterface $childAdmin = null)
    {
        /** @var Page $page */
        $page = $this->getSubject();
        $pageId = $this->id($page);

        if ($pageId) {
            $menu->addChild('admin.labels.page', [
                'route' => 'admin_megacoders_page_page_edit',
                'routeParameters' => ['id' => $pageId]
            ]);

            if ($page->getType() == Page::PAGE_TYPE_PAGE) {
                $menu->addChild('admin.labels.blocks', [
                    'route' => 'admin_megacoders_page_page_pageblock_list',
                    'routeParameters' => ['id' => $pageId]
                ]);

                $menu->addChild('admin.labels.meta', [
                    'route' => 'admin_megacoders_page_page_meta_list',
                    'routeParameters' => ['id' => $pageId]
                ]);

                if ($childAdmin !== null) {
                    switch ($childAdmin->getCode()) {
                        case 'page.admin.page_block':
                            $menu['admin.labels.blocks']->setCurrent(true);
                            break;

                        case 'page.admin.meta':
                            $menu['admin.labels.meta']->setCurrent(true);
                            break;
                    }
                }
            }

        } else {
            /** @var SiteManager $siteManager */
            $siteManager = $this->getConfigurationPool()->getContainer()->get('page.manager.site_manager');
            $sites = $siteManager->loadAll();
            $siteId = $this->getSiteId();

            foreach ($sites as $site) {
                $menu->addChild($site->getName(), [
                    'current' => $siteId == $site->getId(),
                    'route' => 'admin_megacoders_page_page_select_site',
                    'routeParameters' => ['id' => $site->getId()],
                    'extras' => ['hint' => $site->getHost()]
                ]);
            }
        }
    }

    /**
     * {@inheritdoc}
     */
    protected function configureListFields(ListMapper $listMapper)
    {
        $listMapper
            ->add('name', null, [
                'label' => 'admin.entities.page.name',
                'sortable' => false,
                'template' => 'MegacodersPageBundle:Admin/Page:list__field__tree_name.html.twig'
            ])
            ->add('locale', null, [
                'label' => 'admin.entities.page.locale',
                'sortable' => false
            ])
            ->add('url', null, [
                'label' => 'admin.entities.page.url',
                'sortable' => false
            ])
            ->add('sortOrder', null, [
                'label' => 'admin.entities.page.sort_order',
                'sortable' => false
            ])
            ->add('_action', null, [
                'label' => 'admin.actions._action',
                'actions' => [
                    'blocks' => ['template' => 'MegacodersPageBundle:Admin/Page:list__action__blocks.html.twig'],
                    'edit' => [],
                    'clone' => ['template' => 'MegacodersAdminBundle::list__action__clone.html.twig'],
                    'delete' => []
                ]
            ])
        ;
    }

    /**
     * {@inheritdoc}
     */
    protected function configureFormFields(FormMapper $formMapper)
    {
        /** @var SiteManager $siteManager */
        $siteManager = $this->getConfigurationPool()->getContainer()->get('page.manager.site_manager');
        $site = $siteManager->load($this->getSiteId());

        /** @var Page $subject */
        $subject = $this->getSubject();
        $subject->setSite($site);

        /** @var LayoutManager $layoutManager */
        $layoutManager = $this->getConfigurationPool()->getContainer()->get('page.manager.layout_manager');

        $formMapper->with('admin.labels.page')
            ->add('site', null, [
                'label' => 'admin.entities.page.site',
                'disabled' => true
            ])
            ->add('locale', ChoiceType::class, [
                'label' => 'admin.entities.page.locale',
                'required' => true,
                'choices' => $this->getLocaleChoices(),
                'empty_data' => 'admin.labels.locale_like_parent'
            ])
            ->add('parent', PageChoiceType::class, [
                'label' => 'admin.entities.page.parent',
                'required' => false,
                'placeholder' => '— Root —',
                'site_id' => $this->getSiteId(),
                'choice_attr' => function(Page $page) use ($subject) {
                    return [
                        'selected' => $page === $subject->getParent(),
                        'disabled' => $page === $subject || $subject->getChildren()->contains($page)
                    ];
                }
            ])
            ->add('name', null, ['label' => 'admin.entities.page.name',])
            ->add('slug', null, ['label' => 'admin.entities.page.slug'])
            ->add('type', 'sonata_type_choice_field_mask', [
                'label' => 'admin.entities.page.type',
                'choices' => array_flip(Page::PAGE_TYPES_NAMES),
                'map' => [
                    Page::PAGE_TYPE_PAGE => ['layoutId'],
                    Page::PAGE_TYPE_LINK => ['linkType']
                ]
            ])
            ->add('layoutId', ChoiceType::class, [
                'label' => 'admin.entities.page.layout',
                'choices' => $layoutManager->getAll(),
                'choice_label' => 'name',
                'choice_value' => 'id'
            ])
            ->add('linkType', 'sonata_type_choice_field_mask', [
                'label' => 'admin.entities.page.link_type',
                'choices' => array_flip(Page::LINK_TYPES_NAMES),
                'map' => [
                    Page::LINK_TYPE_FIRST_CHILD => [],
                    Page::LINK_TYPE_PAGE => ['linkPage'],
                    Page::LINK_TYPE_URL => ['linkUrl']
                ]
            ])
            ->add('linkPage', PageChoiceType::class, [
                'label' => false, 'required' => false,
                'choice_attr' => function(Page $page) use ($subject) {
                    return [
                        'selected' => $page === ($subject->getLinkPage() ?: $subject->getFirstChild()),
                        'disabled' => $page === $subject
                    ];
                }
            ])
            ->add('linkUrl', null, ['label' => false, 'required' => false])
            ->add('published', null, ['label' => 'admin.entities.page.published'])
        ;

        if ($this->isEditMode()) {
            $formMapper->add('sortOrder', null, ['label' => 'admin.entities.page.sort_order']);

        } else {
            $order = $this->getEntityRepository(Page::class)
                ->createQueryBuilder('p')
                    ->select('max(p.sortOrder)')
                    ->getQuery()
                        ->getSingleScalarResult()
            ;

            $formMapper->add('sortOrder', null, [
                'label' => 'admin.entities.page.sort_order',
                'data' => $order + 10
            ]);
        }

        $formMapper->end();

        $formMapper->get('layoutId')->addModelTransformer(new StringToLayoutTransformer($layoutManager));
    }

    /**
     * @return array
     */
    protected function getLocaleChoices()
    {
        $container = $this->getConfigurationPool()->getContainer();
        $locales = [
            $container->get('translator')->trans('admin.labels.locale_like_parent') => 0
        ];

        foreach ($container->getParameter('megacoders_page.locales') as $locale) {
            $locales[$locale] = $locale;
        }

        return $locales;
    }

    /**
     * @param Page $page
     */
    private function checkPageLocale(Page $page)
    {
        if (!$page->getLocale()) {
            $locale = $page->getParent()
                ? $page->getParent()->getLocale()
                : $this->getConfigurationPool()->getContainer()->getParameter('megacoders_page.default_locale');

            $page->setLocale($locale);
        }
    }

    /**
     * @param Page $page
     */
    private function checkPagePublished(Page $page)
    {
        /** @var array $oldPageData */
        $oldPageData = $this->getEntityManager()->getUnitOfWork()->getOriginalEntityData($page);

        if ($page->isPublished() && (!isset($oldPageData['published']) || $page->isPublished() != $oldPageData['published'])) {
            $page->setPublishedAt(new \DateTime());
        }
    }

}
