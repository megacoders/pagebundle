<?php

namespace Megacoders\PageBundle\Controller\Admin;

use Megacoders\AdminBundle\Controller\BaseAdminController;
use Megacoders\PageBundle\Entity\Block;
use Megacoders\PageBundle\Manager\ModuleManager;
use Symfony\Bridge\Twig\Extension\FormExtension;
use Symfony\Component\Form\Form;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Translation\TranslatorInterface;

class BlockAdminController extends BaseAdminController
{
    /**
     * @param Request $request
     * @return JsonResponse
     */
    public function moduleActionsAction(Request $request)
    {
        $id = $request->get($this->admin->getIdParameter());
        $block = $id ? $this->admin->getObject($id) : null;
        $blockActionId = $block ? $block->getActionId() : null;

        /** @var ModuleManager $moduleManager */
        $moduleManager = $this->get('page.manager.module_manager');
        $moduleId = $request->get('moduleId');

        /** @var TranslatorInterface $translator */
        $translator = $this->get('translator');

        $result = [];

        if ($module = $moduleManager->get($moduleId)) {
            $actions = $module->getActions();

            foreach ($actions as $action) {
                $actionId = $action->getId();
                $result[$actionId] = [
                    'name' => $translator->trans($action->getName()),
                    'selected' => $actionId == $blockActionId
                ];
            }
        }

        return new JsonResponse($result);
    }

    /**
     * @param Request $request
     * @return Response
     */
    public function moduleActionParametersAction(Request $request)
    {
        $request->attributes->set('_form', 'parameters');

        $id = $request->get($this->admin->getIdParameter());

        /** @var Block $block */
        $block = $id ? $this->admin->getObject($id) : $this->admin->getNewInstance();
        $block
            ->setModuleId($request->get('moduleId'))
            ->setActionId($request->get('actionId'))
        ;

        $this->admin->setUniqid($request->get('uniqid'));
        $this->admin->setSubject($block);

        /** @var $form Form */
        $form = $this->admin->getForm();
        $form->setData($block);
        $view = $form->createView();

        $this->get('twig')->getExtension(FormExtension::class)->renderer->setTheme($view, $this->admin->getFormTheme());

        return $this->render($this->admin->getTemplate('edit_parameters'), array(
            'action' => 'edit',
            'form' => $view,
            'object' => $block,
        ), null);
    }
}
