<?php

namespace Megacoders\PageBundle\Controller\Admin;

use Doctrine\Common\Collections\ArrayCollection;
use Megacoders\AdminBundle\Controller\BaseAdminController;
use Megacoders\PageBundle\Admin\SiteAdmin;
use Megacoders\PageBundle\Entity\Site;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Session\Session;

class SiteAdminController extends BaseAdminController
{
    /** @var SiteAdmin */
    protected $admin;

    /**
     * @param Request $request
     * @param Site $object
     * @return null
     */
    protected function preDelete(Request $request, $object)
    {
        /** @var Session $session */
        $session = $this->get('session');

        if ($object->isMain()) {
            $session->getFlashBag()->add('sonata_flash_error', 'You can not remove the main site.');

            return new RedirectResponse($this->admin->generateUrl('list'));
        }

        $session->remove(Site::ADMIN_SESSION_PARAMETER_NAME);
        $object->setPages(new ArrayCollection());
        $object->setMainPage(null);

        return null;
    }
}
