<?php

namespace Megacoders\PageBundle\Controller\Admin;

use Megacoders\AdminBundle\Controller\BaseAdminController;
use Megacoders\PageBundle\Admin\PageAdmin;
use Megacoders\PageBundle\Entity\Site;
use Megacoders\PageBundle\Manager\PageManager;
use Megacoders\PageBundle\Manager\SiteManager;
use Sonata\AdminBundle\Datagrid\ProxyQueryInterface;
use Sonata\AdminBundle\Exception\ModelManagerException;
use Symfony\Bridge\Twig\Extension\FormExtension;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Session\Session;

class PageAdminController extends BaseAdminController
{
    /**
     * @var PageAdmin
     */
    protected $admin;

    /**
     * {@inheritdoc}
     */
    public function listAction(Request $request = null)
    {
        if (!$request->get('filter')) {
            return new RedirectResponse($this->admin->generateUrl('tree'));
        }

        return parent::listAction();
    }

    /**
     * @param Request|null $request
     * @return Response
     */
    public function treeAction(Request $request = null)
    {
        $this->admin->checkAccess('tree');

        /** @var PageManager $pageManager */
        $pageManager = $this->get('page.manager.page_manager');

        $datagrid = $this->admin->getDatagrid();
        $formView = $datagrid->getForm()->createView();

        $this->get('twig')->getExtension(FormExtension::class)->renderer->setTheme($formView, $this->admin->getFormTheme());

        return $this->render($this->admin->getTemplate('tree'), array(
            'action' => 'tree',
            'site' => $this->getSite(),
            'pages' => $pageManager->getTree($this->admin->getSiteId()),
            'form' => $formView,
            'csrf_token' => $this->getCsrfToken('sonata.batch'),
        ));
    }

    /**
     * @param Request|null $request
     * @return RedirectResponse
     */
    public function selectSiteAction(Request $request = null)
    {
        if ($siteId = $request->get('id')) {
            $this->admin->storeSiteId($siteId);
        }

        return new RedirectResponse($this->admin->generateUrl('list'));
    }

    /**
     * {@inheritdoc}
     */
    public function batchActionDelete(ProxyQueryInterface $query)
    {
        $this->admin->checkAccess('batchDelete');

        $modelManager = $this->admin->getModelManager();

        try {
            $modelManager->batchDelete($this->admin->getClass(), $query);
            $this->addFlash('sonata_flash_success', 'flash_batch_delete_success');

        } catch (ModelManagerException $e) {
            $this->handleModelManagerException($e);
            $this->addFlash('sonata_flash_error', 'flash_batch_delete_error');
        }

        $isTreeBatch = $this->getRequest()->get('is_tree_batch');

        return new RedirectResponse($this->admin->generateUrl(
            $isTreeBatch ? 'tree' : 'list',
            ['filter' => $this->admin->getFilterParameters()]
        ));
    }

    /**
     * {@inheritdoc}
     */
    protected function getCloneRedirectUrl($object)
    {
        return $this->admin->generateObjectUrl('edit', $object);
    }

    /**
     * @return Site|null
     */
    protected function getSite()
    {
        /** @var Session $session */
        $session = $this->get('session');

        /** @var SiteManager $siteManager */
        $siteManager = $this->get('page.manager.site_manager');

        $siteId = $session->get(Site::ADMIN_SESSION_PARAMETER_NAME);

        if ($siteId === null) {
            $site = $siteManager->loadMain();

            if ($site !== null) {
                return $site;
            }
        }

        return $siteManager->load($siteId);
    }

}
