<?php

namespace Megacoders\PageBundle\Controller\Admin;

use Doctrine\ORM\NonUniqueResultException;
use Doctrine\ORM\Query\Expr;
use Megacoders\AdminBundle\Controller\BaseAdminController;
use Megacoders\PageBundle\Entity\Block;
use Megacoders\PageBundle\Entity\Page;
use Megacoders\PageBundle\Entity\PageBlock;
use Megacoders\PageBundle\Manager\LayoutManager;
use Megacoders\PageBundle\Manager\ModuleManager;
use Megacoders\PageBundle\Manager\PageManager;
use Sonata\AdminBundle\Admin\Pool;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Translation\TranslatorInterface;

class PageBlockAdminController extends BaseAdminController
{
    /**
     * {@inheritdoc}
     */
    public function listAction(Request $request = null)
    {
        if (!$request->get('filter')) {
            return new RedirectResponse($this->admin->generateUrl('grid'));
        }

        return parent::listAction();
    }

    /**
     * @param Request|null $request
     * @return Response
     * @throws NonUniqueResultException
     */
    public function gridAction(Request $request = null)
    {
        $this->admin->checkAccess('grid');

        $id = $request->get($this->admin->getParent()->getIdParameter());

        /** @var Pool $adminPool */
        $adminPool = $this->get('sonata.admin.pool');

        /** @var PageManager $pageManager */
        $pageManager = $this->get('page.manager.page_manager');

        /** @var LayoutManager $layoutManager */
        $layoutManager = $this->get('page.manager.layout_manager');

        /** @var ModuleManager $moduleManager */
        $moduleManager = $this->get('page.manager.module_manager');

        /** @var Page $page */
        $page = $pageManager->load($id);
        $layout = $layoutManager->get($page->getLayoutId());

        $rowWidth = 0;
        $rows = [];
        $row = [];

        foreach ($layout->getAreas(false) as $area) {
            if ($rowWidth >= 12) {
                $rows[] = $row;
                $row = [];
                $rowWidth = 0;
            }

            $pageBlock = $page->getBlock($area->getId());
            $block = $pageBlock ? $pageBlock->getBlock() : null;
            $module = $block ? $moduleManager->get($block->getModuleId()) : null;
            $action = $module ? $module->getAction($block->getActionId()) : null;
            $contentDescription = null;

            if ($module !== null) {
                $contentEntityDescription = $module->getContentEntityDescription();

                if ($contentEntityDescription !== null && $contentEntityClass = $contentEntityDescription->getClass()) {
                    $contentAdmin = $adminPool->getAdminByClass($contentEntityClass);

                    $contentAdminRoute = 'list';
                    $contentAdminParameters = ['filter' => []];

                    foreach ($contentEntityDescription->getFilter() as $parameter) {
                        $contentAdminParameters['filter'][$parameter]['value'] = $block->getParameter($parameter);
                    }

                    if ($contentEntityDescription->getIdParameter() && $contentId = $block->getParameter($contentEntityDescription->getIdParameter())) {
                        $contentAdminParameters['id'] = $contentId;
                        $contentAdminRoute = 'edit';

                        $contentObject = $this->getEntityRepository($contentEntityClass)->find($contentId);

                        $contentDescription['name'] = method_exists($contentObject, '__toString')
                            ? $contentObject->__toString()
                            : implode('#', [$this->trans($contentAdmin->getLabel()), $contentId]);

                    } else {
                        $contentDescription['name'] = $this->trans($contentAdmin->getLabel());
                    }

                    $contentDescription['url'] = ($contentEntityRoute = $contentEntityDescription->getRoute())
                        ? $this->generateUrl($contentEntityRoute, $contentAdminParameters)
                        : $contentAdmin->generateUrl($contentAdminRoute, $contentAdminParameters);
                }
            }

            $rowWidth += $area->getWidth();
            $row[] = [
                'area' => $area,
                'block' => $pageBlock,
                'module' => $module,
                'action' => $action,
                'contentDescription' => $contentDescription
            ];
        }

        $rows[] = $row;

        $datagrid = $this->admin->getDatagrid();
        $formView = $datagrid->getForm()->createView();

        $this->get('twig')->getExtension('form')->renderer->setTheme($formView, $this->admin->getFormTheme());

        return $this->render($this->admin->getTemplate('grid'), array(
            'action' => 'grid',
            'page' => $page,
            'rows' => $rows,
            'form' => $formView,
            'csrf_token' => $this->getCsrfToken('sonata.batch'),
        ));
    }

    /**
     * @param Request $request
     * @return JsonResponse
     */
    public function moduleBlocksAction(Request $request)
    {
        $id = $request->get($this->admin->getIdParameter());

        /** @var PageBlock $object */
        $object = $id ? $this->admin->getObject($id) : null;
        $block = $object ? $object->getBlock() : null;
        $objectBlockId = $block ? $block->getId() : null;

        $moduleId = $request->get('moduleId');

        $expr = new Expr();
        $queryBuilder = $this->getEntityRepository(Block::class)->createQueryBuilder('b')
            ->orderBy('b.name', 'ASC')
        ;

        /** @var Block[] $blocks */
        if (!!$moduleId) {
            $queryBuilder->where($expr->eq('b.moduleId', $expr->literal($moduleId)));
        }

        $blocks = $queryBuilder->getQuery()->getResult();
        $result = [];

        foreach ($blocks as $block) {
            $blockId = $block->getId();

            $result[$blockId] = [
                'name' => $block->getName(),
                'selected' => $blockId == $objectBlockId
            ];
        }

        return new JsonResponse($result);
    }

    /**
     * @param Request $request
     * @return JsonResponse
     */
    public function moduleTemplatesAction(Request $request)
    {
        $id = $request->get($this->admin->getIdParameter());

        /** @var PageBlock $pageBlock */
        $pageBlock = $id ? $this->admin->getObject($id) : null;
        $templateId = $pageBlock ? $pageBlock->getTemplateId() : null;

        /** @var ModuleManager $moduleManager */
        $moduleManager = $this->get('page.manager.module_manager');
        $moduleId = $request->get('moduleId');

        /** @var TranslatorInterface $translator */
        $translator = $this->get('translator');

        $result = [];

        if ($module = $moduleManager->get($moduleId)) {
            $templates = $module->getTemplates();

            foreach ($templates as $template) {
                $id = $template->getId();
                $result[$id] = [
                    'name' => $translator->trans($template->getName()),
                    'selected' => $id == $templateId
                ];
            }
        }

        return new JsonResponse($result);
    }

}
