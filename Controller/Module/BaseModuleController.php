<?php

namespace Megacoders\PageBundle\Controller\Module;

use Megacoders\PageBundle\Entity\Page;
use Megacoders\PageBundle\Entity\PageBlock;
use Megacoders\PageBundle\Model\LayoutArea;
use Megacoders\PageBundle\Model\LoadedPage;
use Megacoders\PageBundle\Model\ModuleTemplate;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\Config\FileLocatorInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Exception\RouteNotFoundException;
use Symfony\Component\Routing\Generator\UrlGeneratorInterface;
use Symfony\Component\Routing\RouteCollection;

abstract class BaseModuleController extends Controller implements ModuleControllerInterface, RoutesConfigurableInterface
{
    /**
     * @var string
     */
    protected $mainBlockRoute;

    /**
     * @var array
     */
    protected $blockRoutes;

    /**
     * @var string
     */
    private $moduleId;

    /**
     * @var array
     */
    private $parameters;

    /**
     * @var Request
     */
    private $request;

    /**
     * @var ContainerInterface
     */
    protected $container;

    /**
     * @return string
     */
    public function getModuleId()
    {
        return $this->moduleId;
    }

    /**
     * @param $moduleId
     * @return BaseModuleController
     */
    public function setModuleId($moduleId)
    {
        $this->moduleId = $moduleId;
        return $this;
    }

    /**
     * @return array
     */
    public function getModuleParameters()
    {
        return $this->parameters;
    }

    /**
     * @param string $name
     * @param mixed|null $default
     * @return mixed|null
     */
    public function getModuleParameter($name, $default = null)
    {
        return isset($this->parameters[$name]) ? $this->parameters[$name] : $default;
    }

    /**
     * @param array $parameters
     * @return BaseModuleController
     */
    public function setModuleParameters(array $parameters)
    {
        $this->parameters = [];

        foreach ($parameters as $name => $value) {
            $this->setModuleParameter($name, $value);
        }

        return $this;
    }

    /**
     * @param string $name
     * @param mixed $value
     * @return BaseModuleController
     */
    public function setModuleParameter($name, $value)
    {
        $this->parameters[$name] = $value;
        return $this;
    }

    /**
     * @param string $baseName
     * @param string $baseUrl
     * @param PageBlock $pageBlock
     * @return RouteCollection|void
     */
    public function configureRoutes($baseName, $baseUrl, PageBlock $pageBlock)
    {
    }

    /**
     * @return RouteCollection|void
     */
    public function configureServiceRoutes()
    {
    }

    /**
     * @param Request $request
     * @return Response
     */
    public function handle(Request $request) {
        $this->setModuleParameters($request->get('_parameters', []));

        $this->request = $request;

        if ($this->getActionId()) {
            $method = strtolower($this->getActionId()) .'Action';

            if (method_exists($this, $method)) {
                return $this->{$method}($request);
            }
        }

        return $this->indexAction($request);
    }

    /**
     * @param string $routeNamePostfix
     * @param array $parameters
     * @param int $referenceType
     * @return null|string
     */
    public function generateMainBlockUrl($routeNamePostfix = '', array $parameters = [],
                                         $referenceType = UrlGeneratorInterface::ABSOLUTE_PATH)
    {
        try {
            return $this->generateUrl(
                $this->getBaseRouteName() .($routeNamePostfix ? '_' .$routeNamePostfix : ''),
                $parameters, $referenceType
            );

        } catch (RouteNotFoundException $e) {
            return null;
        }
    }

    /**
     * @param string $actionId
     * @param array $parameters
     * @param int $referenceType
     * @return null|string
     */
    public function generateActionUrl($actionId = 'index', array $parameters = [],
                                      $referenceType = UrlGeneratorInterface::ABSOLUTE_PATH)
    {
        try {
            return $this->generateUrl($this->getActionRouteName($actionId), $parameters, $referenceType);

        } catch (RouteNotFoundException $e) {
            return null;
        }
    }

    public function generateModuleUrl($moduleId, $actionId = 'index', $routeNamePostfix = '', $parameters = [],
                                      $referenceType = UrlGeneratorInterface::ABSOLUTE_PATH)
    {
        try {
            return $this->generateUrl(
                $this->getModuleRouteName($moduleId, $actionId) .($routeNamePostfix ? '_' .$routeNamePostfix : ''),
                $parameters, $referenceType);

        } catch (RouteNotFoundException $e) {
            return null;
        }
    }

    /**
     * @return string
     */
    protected function getActionId()
    {
        return $this->request->get('_action');
    }

    /**
     * @return string
     */
    protected function getTemplateId()
    {
        return $this->request->get('_template');
    }

    /**
     * @param string $action
     * @return string | null
     */
    protected function getTemplate($action)
    {
        /** @var FileLocatorInterface $fileLocator */
        $fileLocator = $this->get('file_locator');

        $class = get_class($this);
        $bundle = str_replace('\\', '', substr($class, 0, strpos($class, 'Bundle')));
        $templateIds = [$this->getTemplateId(), ModuleTemplate::DEFAULT_TEMPLATE_ID];

        foreach ($templateIds as $templateId) {
            try {
                $templatePath = sprintf('%sBundle/views/%s/%s.html.twig', $bundle, $templateId, $action);
                return $fileLocator->locate($templatePath);

            } catch (\InvalidArgumentException $e) {
                continue;
            }
        }

        return $fileLocator->locate(sprintf('%sBundle/views/%s.html.twig', $bundle, $action));
    }

    /**
     * {@inheritdoc}
     */
    protected function render($actionName, array $parameters = array(), Response $response = null)
    {
        $view = $this->getTemplate($actionName);
        $info = array_merge([
            '_page' => $this->getPage(),
            '_area' => $this->getArea(),
            '_template' => $this->getTemplateId(),
            '_parameters' => $this->getModuleParameters(),
            'title' => $this->getBlockTitle()
        ], $parameters);

        /** @var \Twig_Environment $twig */
        $twig = $this->container->has('twig') ? $this->container->get('twig') : null;
        $twigIsStrictVariables = $twig ? $twig->isStrictVariables() : null;

        if ($twigIsStrictVariables === true) {
            $twig->disableStrictVariables();
        }

        $response = parent::render($view, $info, $response);

        if ($twigIsStrictVariables === true) {
            $twig->enableStrictVariables();
        }

        return $response;
    }

    /**
     * @return string
     */
    protected function getBaseRouteName()
    {
        if (!$this->isMain()) {
            $moduleId = $this->getModuleId();

            if (isset($this->mainBlockRoute)) {
                return $this->mainBlockRoute;
            }

            /** @var Page $page */
            $page = $this->get('page.manager.page_manager')
                ->getModuleBasePage($moduleId, $this->getLocale());

            if ($page !== null) {
                $this->mainBlockRoute = $page->getRouteName();
                return $this->mainBlockRoute;
            }
        }

        return $this->request->get('_baseRoute');
    }

    /**
     * @param string $moduleId
     * @param string $actionId
     * @return string|null
     */
    protected function getModuleRouteName($moduleId, $actionId = 'index')
    {
        $routeNameKey = implode(':', [$moduleId, $actionId]);

        if (isset($this->blockRoutes[$routeNameKey])) {
            return $this->blockRoutes[$routeNameKey];
        }

        /** @var Page $page */
        $page = $this->get('page.manager.page_manager')
            ->getModuleActionPage($moduleId, $actionId, $this->getLocale());

        if ($page !== null) {
            $this->blockRoutes[$routeNameKey] = $page->getRouteName();
            return $this->blockRoutes[$routeNameKey];
        }

        $this->blockRoutes[$routeNameKey] = null;

        return null;
    }

    /**
     * @param string $actionId
     * @return string|null
     */
    protected function getActionRouteName($actionId)
    {
        return $this->getModuleRouteName($this->getModuleId(), $actionId);
    }

    /**
     * @return string
     */
    protected function getRouteName()
    {
        return $this->request->get('_route');
    }

    /**
     * @return LoadedPage
     */
    protected function getPage()
    {
        return $this->request->get('_page');
    }

    /**
     * @return LayoutArea
     */
    protected function getArea()
    {
        return $this->request->get('_area');
    }

    /**
     * @return string
     */
    protected function getBlockTitle()
    {
        return $this->request->get('_block_title');
    }

    /**
     * @return bool
     */
    protected function isMain()
    {
        return $this->getArea()->isMain();
    }

    /**
     * @return string
     */
    protected function getLocale()
    {
        return $this->request->get('_locale');
    }

    /**
     * @param mixed $object
     */
    protected function addPageExtraBreadcrumb($object)
    {
    }

    /**
     * @param mixed $object
     */
    protected function addPageMetaTitle($object)
    {
    }

    /**
     * @param Request $request
     * @return Response
     */
    abstract public function indexAction(Request $request);

}
