<?php

namespace Megacoders\PageBundle\Controller\Module;

use Megacoders\PageBundle\Entity\PageBlock;
use Symfony\Component\Routing\RouteCollection;

interface RoutesConfigurableInterface
{
    /**
     * @param string $baseName
     * @param string $baseUrl
     * @param PageBlock $block
     * @return RouteCollection|void
     */
    function configureRoutes($baseName, $baseUrl, PageBlock $block);

    /**
     * @return RouteCollection|void
     */
    function configureServiceRoutes();
}
