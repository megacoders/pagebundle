<?php

namespace Megacoders\PageBundle\Controller\Module;

use Megacoders\PageBundle\Model\ModuleParameter;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

interface ModuleControllerInterface
{
    /**
     * @param ContainerInterface $container
     */
    function setContainer(ContainerInterface $container);

    /**
     * @return string
     */
    function getModuleId();

    /**
     * @param string
     * @return ModuleControllerInterface
     */
    function setModuleId($moduleId);

    /**
     * @return ModuleParameter[]
     */
    function getModuleParameters();

    /**
     * @param string $name
     * @param null $default
     * @return mixed
     */
    function getModuleParameter($name, $default = null);

    /**
     * @param ModuleParameter[] $parameters
     * @return ModuleControllerInterface
     */
    function setModuleParameters(array $parameters);

    /**
     * @param string $name
     * @param mixed $value
     * @return ModuleControllerInterface
     */
    function setModuleParameter($name, $value);

    /**
     * @param Request $request
     * @return Response
     */
    function handle(Request $request);
}
