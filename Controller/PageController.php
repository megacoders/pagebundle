<?php

namespace Megacoders\PageBundle\Controller;

use Megacoders\PageBundle\Http\ErrorResponse;
use Megacoders\PageBundle\Manager\PageManager;
use Megacoders\PageBundle\Model\LoadedPage;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;

class PageController extends Controller
{
    /**
     * @param Request $request
     * @return Response
     */
    public function pageAction(Request $request)
    {
        $loadedPage = $this->loadPage($request->get('_pageId'));
        $loadedPage->handle($request);

        $response = $loadedPage->getResponse();

        if ($response instanceof ErrorResponse) {
            return $this->render('MegacodersPageBundle:Layout:error.html.twig', [
                'statusCode' => $response->getStatusCode(),
                'exception' => $response->getException()
            ], $response);
        }

        if ($request->isXmlHttpRequest()) {
            return $loadedPage->getResponse();
        }

        return $this->renderPage($loadedPage);
    }

    /**
     * @param LoadedPage $loadedPage
     * @return Response
     */
    public function renderPage(LoadedPage $loadedPage)
    {
        return $this->render($loadedPage->getTemplate(), ['page' => $loadedPage], $loadedPage->getResponse());
    }

    /**
     * @param Request $request
     * @return Response
     */
    public function blockAction(Request $request)
    {
        $loadedPage = $this->loadPage($request->get('_pageId'));
        $loadedBlock = $loadedPage->getBlock($request->get('_areaId'));

        $locale = $loadedPage->getPage()->getLocale();
        $request->setLocale($locale);

        if ($loadedBlock === null) {
            throw new NotFoundHttpException('Block not found');
        }

        $request->attributes->set('_page', $loadedPage);
        $request->attributes->set('_baseRoute', $loadedPage->getPage()->getRouteName());
        $request->attributes->set('_locale', $locale);
        
        return $loadedBlock->handle($request);
    }

    /**
     * @param int $pageId
     * @return LoadedPage
     */
    private function loadPage($pageId) {
        /** @var PageManager $pageManager */
        $pageManager = $this->get('page.manager.page_manager');
        $loadedPage = $pageManager->load($pageId, true);

        if ($loadedPage === null) {
            throw new NotFoundHttpException('Page not found');
        }

        return $loadedPage;
    }
}
