<?php

namespace Megacoders\PageBundle\DependencyInjection\Compiler;

use Symfony\Component\DependencyInjection\Compiler\CompilerPassInterface;
use Symfony\Component\DependencyInjection\ContainerBuilder;

class LayoutPass implements CompilerPassInterface
{
    /**
     * {@inheritdoc}
     */
    public function process(ContainerBuilder $container)
    {
        if (!$container->has('page.manager.layout_manager')) {
            return;
        }

        $managerDefinition = $container->findDefinition('page.manager.layout_manager');
        $layouts = $container->getParameter('page.layouts');

        if (is_array($layouts)) {
            foreach ($layouts as $id => $properties) {
                $managerDefinition->addMethodCall('registerLayout', [$id, $properties]);
            }
        }
    }
}
