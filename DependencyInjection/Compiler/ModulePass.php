<?php

namespace Megacoders\PageBundle\DependencyInjection\Compiler;

use Symfony\Component\DependencyInjection\Compiler\CompilerPassInterface;
use Symfony\Component\DependencyInjection\ContainerBuilder;
use Symfony\Component\DependencyInjection\Reference;

class ModulePass implements CompilerPassInterface
{
    /**
     * {@inheritdoc}
     */
    public function process(ContainerBuilder $container)
    {
        if (!$container->has('page.manager.module_manager')) {
            return;
        }

        $managerDefinition = $container->findDefinition('page.manager.module_manager');
        $taggedServices = $container->findTaggedServiceIds('page.module');

        foreach ($taggedServices as $id => $tags) {
            $moduleDefinition = $container->findDefinition($id);
            $managerDefinition->addMethodCall('registerModule', [
                $id,
                new Reference($id),
                $moduleDefinition->getProperties()
            ]);
        }
    }
}
