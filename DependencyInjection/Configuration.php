<?php
namespace Megacoders\PageBundle\DependencyInjection;

use Symfony\Component\Config\Definition\Builder\TreeBuilder;
use Symfony\Component\Config\Definition\ConfigurationInterface;

class Configuration implements ConfigurationInterface
{
    public function getConfigTreeBuilder()
    {
        $treeBuilder = new TreeBuilder();
        $rootNode = $treeBuilder->root('megacoders_page');

        $rootNode
            ->children()
                ->arrayNode('locales')
                    ->cannotBeEmpty()
                    ->prototype('variable')->end()
                ->end()
                ->scalarNode('default_locale')
                    ->cannotBeEmpty()
                ->end()
            ->end()
        ;

        return $treeBuilder;
    }
}
