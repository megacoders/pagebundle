<?php

namespace Megacoders\PageBundle\Manager;

use Megacoders\PageBundle\Model\Layout;
use Megacoders\PageBundle\Model\LayoutArea;

class LayoutManager
{
    const DEFAULT_TEMPLATE = 'MegacodersPageBundle:Layout:base.html.twig';

    /**
     * @var Layout[]
     */
    private $layouts = null;

    /**
     * @param string $id
     * @param array $properties
     */
    public function registerLayout($id, array $properties = [])
    {
        $layout = new Layout();
        $layout
            ->setId($id)
            ->setName(isset($properties['name']) ? $properties['name'] : $id)
            ->setHidden(isset($properties['hidden']))
            ->setTemplate(isset($properties['template']) ? $properties['template'] : self::DEFAULT_TEMPLATE)
        ;

        if (!empty($properties['areas'])) {
            foreach ($properties['areas'] as $areaId => $areaProperties) {
                $area = new LayoutArea();
                $area
                    ->setId($areaId)
                    ->setName(isset($areaProperties['name']) ? $areaProperties['name'] : $areaId)
                    ->setMain(isset($areaProperties['main']) && !!$areaProperties['main'])
                    ->setWidth(isset($areaProperties['width']) ? $areaProperties['width'] : 1)
                    ->setHeight(isset($areaProperties['height']) ? $areaProperties['height'] : 1)
                ;

                $layout->addArea($area);
            }
        }

        $this->layouts[$id] = $layout;
    }

    /**
     * @return Layout[]
     */
    public function getAll()
    {
        if ($this->layouts === null) {
            return [];
        }

        return array_filter($this->layouts, function(Layout $layout) {
            return !$layout->isHidden();
        });
    }

    /**
     * @param string $id
     * @return Layout|null
     */
    public function get($id)
    {
        if (!$id) return null;

        return isset($this->layouts[$id]) ? $this->layouts[$id] : null;
    }
}
