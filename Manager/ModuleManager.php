<?php

namespace Megacoders\PageBundle\Manager;

use Doctrine\Common\Collections\ArrayCollection;
use Megacoders\PageBundle\Controller\Module\ModuleControllerInterface;
use Megacoders\PageBundle\Model\ContentEntityDescription;
use Megacoders\PageBundle\Model\Module;
use Megacoders\PageBundle\Model\ModuleAction;
use Megacoders\PageBundle\Model\ModuleParameter;
use Megacoders\PageBundle\Model\ModuleTemplate;
use Symfony\Component\DependencyInjection\ContainerInterface;

class ModuleManager
{

    /**
     * @var Module[]
     */
    private $modules = null;

    /**
     * @var ContainerInterface
     */
    private $container;

    /**
     * ModuleManager constructor.
     * @param ContainerInterface $container
     */
    public function __construct(ContainerInterface $container)
    {
        $this->container = $container;
    }

    /**
     * @param string $id
     * @param ModuleControllerInterface $controller
     * @param array $properties
     */
    public function registerModule($id, ModuleControllerInterface $controller, array $properties = [])
    {
        $controller
            ->setModuleId($id)
            ->setContainer($this->container)
        ;

        $module = new Module();
        $module
            ->setId($id)
            ->setName(isset($properties['name']) ? $properties['name'] : $id)
            ->setController($controller)
        ;

        if (isset($properties['content_entity'])) {
            $module->setContentEntityDescription(new ContentEntityDescription($properties['content_entity']));
        }

        if (!empty($properties['actions'])) {
            foreach ($properties['actions'] as $actionId => $actionProperties) {
                $actionName = isset($actionProperties['name']) ? $actionProperties['name'] : $actionId;
                $action = new ModuleAction($actionId, $actionName);

                if (!empty($actionProperties['parameters'])) {
                    foreach ($actionProperties['parameters'] as $parameterId => $parameterProperties) {
                        $parameterName = isset($parameterProperties['name'])
                            ? $parameterProperties['name']
                            : $parameterId;

                        $parameterType = isset($parameterProperties['type'])
                            ? ModuleParameter::getTypeClass($parameterProperties['type'])
                            : ModuleParameter::DEFAULT_TYPE_CLASS;

                        $parameterOptions = isset($parameterProperties['options'])
                            ? $parameterProperties['options']
                            : [];

                        $action->addParameter(new ModuleParameter(
                            $parameterId, $parameterName, $parameterType, $parameterOptions));
                    }
                }

                $module->addAction($action);
            }

        } else {
            $module->addAction(new ModuleAction(ModuleAction::DEFAULT_ACTION_ID, 'admin.defaults.block.action'));
        }

        if (!empty($properties['templates'])) {
            foreach ($properties['templates'] as $templateId => $name) {
                $module->addTemplate(new ModuleTemplate($templateId, $name));
            }

        } else {
            $module->addTemplate(new ModuleTemplate(
                ModuleTemplate::DEFAULT_TEMPLATE_ID,
                'admin.defaults.block.template'
            ));
        }

        $this->modules[$id] = $module;
    }

    /**
     * @return Module[]
     */
    public function getAll()
    {
        return $this->modules ?: [];
    }

    /**
     * @param string $id
     * @return Module|null
     */
    public function get($id)
    {
        if (!$id) return null;

        return isset($this->modules[$id]) ? $this->modules[$id] : null;
    }

    /**
     * @param string $id
     * @param string $actionId
     * @return ArrayCollection|ModuleParameter[]|null
     */
    public function getModuleParameters($id, $actionId)
    {
        $module = $this->get($id);

        if ($module !== null) {
            $action = $module->getAction($actionId);

            if ($action !== null) {
                return $action->getParameters();
            }
        }

        return null;
    }

}
