<?php

namespace Megacoders\PageBundle\Manager;

use Doctrine\ORM\EntityManager;
use Doctrine\ORM\Query\Expr;
use Doctrine\ORM\QueryBuilder;
use Megacoders\PageBundle\Entity\Page;
use Megacoders\PageBundle\Entity\Site;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\Routing\Route;
use Symfony\Component\Routing\RouteCollection;

class SiteManager
{
    /**
     * @var EntityManager
     */
    private $entityManager;

    /**
     * @var PageManager
     */
    private $pageManager;

    /**
     * SiteManager constructor.
     * @param ContainerInterface $container
     */
    public function __construct(ContainerInterface $container)
    {
        $this->entityManager = $container->get('doctrine.orm.entity_manager');
        $this->pageManager = $container->get('page.manager.page_manager');
    }

    /**
     * @return Site[]
     */
    public function loadAll()
    {
        return $this->getQueryBuilder()
            ->orderBy('s.main', 'DESC')
            ->addOrderBy('s.name', 'ASC')
            ->getQuery()
                ->getResult()
        ;
    }

    /**
     * @param int $id
     * @return Site|null
     */
    public function load($id)
    {
        if (!$id) return null;

        $expr = new Expr();

        return $this->getQueryBuilder()
            ->where($expr->eq('s.id', $id))
            ->getQuery()
                ->getOneOrNullResult()
        ;
    }

    /**
     * @return Site|null
     */
    public function loadMain()
    {
        $expr = new Expr();

        $site = $this->getQueryBuilder()
            ->where($expr->eq('s.main', $expr->literal(true)))
            ->getQuery()
                ->getOneOrNullResult()
        ;

        return $site;
    }

    /**
     * @param string $host
     * @return Site|null
     */
    public function loadByHost($host)
    {
        $expr = new Expr();

        $site = $this->getQueryBuilder()
            ->where($expr->eq('s.host', $expr->literal($host)))
            ->getQuery()
                ->getOneOrNullResult()
        ;

        return $site;
    }

    /**
     * @param Site $site
     * @return RouteCollection
     */
    public function configureRoutes($site)
    {
        $collection = new RouteCollection();
        $mainPage = $site->getMainPage();

        if (!$mainPage) {
            $mainPage = $site->getPages()->filter(function(Page $page) {
                return null == $page->getParent();
            })->first();
        }

        if ($mainPage) {
            $collection->add($site->getRouteName(), new Route($mainPage->getUrl(), [
                '_controller' => 'FrameworkBundle:Redirect:urlRedirect',
                'path' => '/',
                'permanent' => true
            ]));
        }

        $pages = $this->pageManager->getArray($site->getId(), null, null, true);

        /** @var Page $page */
        foreach ($pages as $page) {
            $collection->addCollection($this->pageManager->configureRoutes($page, $page === $mainPage));
        }

        $collection->setSchemes($site->getHostInfo('scheme'));
        $collection->setHost($site->getHostInfo('host'));

        return $collection;
    }

    /**
     * @return QueryBuilder
     */
    protected function getQueryBuilder()
    {
        return $this->entityManager->getRepository(Site::class)
            ->createQueryBuilder('s', 's.id')
                ->addSelect('mp')
                ->leftJoin('s.mainPage', 'mp')
        ;
    }
}
