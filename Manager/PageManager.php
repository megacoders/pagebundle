<?php

namespace Megacoders\PageBundle\Manager;


use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Criteria;
use Doctrine\ORM\EntityManager;
use Doctrine\ORM\NonUniqueResultException;
use Doctrine\ORM\PersistentCollection;
use Doctrine\ORM\Query;
use Doctrine\ORM\Query\Expr;
use Doctrine\ORM\QueryBuilder;
use Gedmo\Translatable\Query\TreeWalker\TranslationWalker;
use Megacoders\PageBundle\Controller\Module\RoutesConfigurableInterface;
use Megacoders\PageBundle\Entity\Page;
use Megacoders\PageBundle\Entity\PageBlock;
use Megacoders\PageBundle\Model\Layout;
use Megacoders\PageBundle\Model\LoadedBlock;
use Megacoders\PageBundle\Model\LoadedPage;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\Routing\Route;
use Symfony\Component\Routing\RouteCollection;

class PageManager
{
    /**
     * @var EntityManager
     */
    private $entityManager;

    /**
     * @var LayoutManager
     */
    private $layoutManager;

    /**
     * @var ModuleManager
     */
    private $moduleManager;

    /**
     * @var bool
     */
    private $debug;

    /**
     * PageManager constructor.
     * @param ContainerInterface $container
     */
    public function __construct(ContainerInterface $container)
    {
        $this->entityManager = $container->get('doctrine.orm.entity_manager');
        $this->layoutManager = $container->get('page.manager.layout_manager');
        $this->moduleManager = $container->get('page.manager.module_manager');
        $this->debug = $container->getParameter('kernel.debug');
    }

    /**
     * @param int|null $siteId
     * @param boolean $onlyPublished
     * @return Page[]
     */
    public function loadAll($siteId = null, $onlyPublished = false)
    {
        $queryBuilder = $this->getQueryBuilder();

        if (!!$siteId) {
            $queryBuilder
                ->where('p.site = :siteId')
                ->setParameter('siteId', $siteId)
                ->orderBy('s.main', 'DESC');
        }

        if ($onlyPublished) {
            $queryBuilder
                ->andWhere('p.published = :isPublished')
                ->setParameter('isPublished', true);
        }

        /** @var Page[] $pages */
        $pages = $queryBuilder
            ->addOrderBy('p.sortOrder')
            ->getQuery()
                ->getResult();

        foreach ($pages as $page) {
            $parent = $page->getParent();
            $children = $page->getChildren();

            if ($children instanceof PersistentCollection) {
                $children->setInitialized(true);
            }

            if ($parent !== null) {
                $key = $parent ? $parent->getId() : null;

                if (isset($pages[$key])) {
                    $pages[$key]->addChildren($page);
                }
            }
        }

        return $pages;
    }

    /**
     * @param int $id
     * @param bool $compose
     * @return Page|LoadedPage|null
     * @throws NonUniqueResultException
     */
    public function load($id, $compose = false)
    {
        if (!$id) return null;

        $queryBuilder = $this->getQueryBuilder();

        if ($compose) {
            $queryBuilder
                ->addSelect('pb, b')
                ->leftJoin('p.blocks', 'pb')
                ->leftJoin('pb.block', 'b');
        }

        $queryBuilder
            ->andWhere('p.id = :id')
            ->setParameter('id', $id);

        /** @var Page $page */
        $page = $queryBuilder->getQuery()
            ->setHint(Query::HINT_CUSTOM_OUTPUT_WALKER, TranslationWalker::class)
            ->getOneOrNullResult();

        if (!$compose || $page === null) {
            return $page;
        }

        return $this->composePage($page);
    }

    /**
     * @param array $ids
     * @param boolean $onlyPublished
     * @return Page[]
     */
    public function loadArray(array $ids, $onlyPublished = false)
    {
        if (!$ids) return [];

        $queryBuilder = $this->getQueryBuilder()
            ->where('p.id in (:ids)')
            ->setParameter('ids', $ids)
            ->addOrderBy('p.sortOrder');

        if ($onlyPublished) {
            $queryBuilder
                ->andWhere('p.published = :isPublished')
                ->setParameter('isPublished', true);
        }

        return $queryBuilder->getQuery()->getResult();
    }

    /**
     * @param int|null $siteId
     * @param int|null $parentId
     * @param boolean $onlyPublished
     * @return Page[]
     */
    public function getTree($siteId = null, $parentId = null, $onlyPublished = false)
    {
        $pages = $this->loadAll($siteId, $onlyPublished);

        if ($parentId !== null) {
            return isset($pages[$parentId]) ? $pages[$parentId]->getChildren()->toArray() : [];
        }

        return array_filter($pages, function(Page $page) {
            return $page->getParent() === null;
        });
    }

    /**
     * @param int|null $siteId
     * @param int|null $parentId
     * @param int|null $depth
     * @param boolean $onlyPublished
     * @return Page[]
     */
    public function getArray($siteId = null, $parentId = null, $depth = null, $onlyPublished = false)
    {
        return $this->treeToArray($this->getTree($siteId, $parentId, $onlyPublished), $depth);
    }

    /**
     * @param Page[] $pages
     * @param int|null $depth
     * @return Page[]
     */
    protected function treeToArray(array $pages, $depth = null)
    {
        if ($depth !== null) {
            $depth -= 1;
        }

        $tree = [];

        /** @var Page $page */
        foreach ($pages as $page) {
            $tree[] = $page;
            $children = $page->getChildren();

            if (($depth == null || $depth > 0) && $children->count() > 0) {
                $tree = array_merge($tree, $this->treeToArray($children->toArray(), $depth ?: null));
            }
        }

        return $tree;
    }

    /**
     * @param Page $page
     * @param bool $isMain
     * @return RouteCollection
     */
    public function configureRoutes($page, $isMain = false)
    {
        $collection = new RouteCollection();
        $baseName = $page->getRouteName();

        if ($page->getType() == Page::PAGE_TYPE_LINK) {
            $collection->add($baseName, new Route($page->getPath(), [
                '_controller' => 'FrameworkBundle:Redirect:urlRedirect',
                'path' => $page->getTargetUrl()
            ]));

            return $collection;
        }

        $baseUrl = ($page->isPublished() ? '' : '/unpublished') .($isMain ? '/' : $page->getUrl());

        $routeDefaults = [
            '_controller' => 'MegacodersPageBundle:Page:page',
            '_pageId' => $page->getId(),
            '_pageParentIds' => $this->getPageParentIds($page),
            '_baseRoute' => $baseName,
            '_locale' => $page->getLocale()
        ];

        $collection->add($baseName, new Route($baseUrl, $routeDefaults));

        $layout = $this->layoutManager->get($page->getLayoutId());

        if ($layout === null) {
            return $collection;
        }

        $blocks = $page->getBlocks();

        /** @var PageBlock $block */
        foreach ($blocks as $block) {
            $area = $layout->getArea($block->getAreaId());

            if ($area !== null && $area->isMain()) {
                $module = $this->moduleManager->get($block->getModuleId());

                if ($module !== null) {
                    $controller = $module->getController();

                    if ($controller instanceof RoutesConfigurableInterface) {
                        $moduleRoutes = $controller->configureRoutes($baseName, $baseUrl, $block);

                        if ($moduleRoutes instanceof RouteCollection) {
                            $moduleRoutes->addDefaults($routeDefaults);

                            return $moduleRoutes;
                        }
                    }
                }

                break;
            }
        }

        return $collection;
    }

    /**
     * @return QueryBuilder
     */
    protected function getQueryBuilder()
    {
        return $this->entityManager->getRepository(Page::class)
            ->createQueryBuilder('p', 'p.id')
            ->addSelect('s, pm, pc')
            ->leftJoin('p.site', 's')
            ->leftJoin('p.metas', 'pm')
            ->leftJoin('p.children', 'pc');
    }

    /**
     * @param Page $page
     * @return array
     */
    protected function getPageParentIds(Page $page)
    {
        $ids = [];

        while ($page = $page->getParent()) {
            $ids[] = $page->getId();
        }

        return $ids;
    }

    /**
     * @param Page $page
     * @return LoadedPage
     */
    protected function composePage(Page $page)
    {
        $layout = $this->layoutManager->get($page->getLayoutId());

        if ($layout === null) {
            $layout = $this->layoutManager->get('page.layout.__fallback__');
        }

        $pageBlocks = $page->getBlocks();
        $loadedBlocks = new ArrayCollection();

        foreach ($pageBlocks as $pageBlock) {
            $area = $layout->getArea($pageBlock->getAreaId());
            $block = $pageBlock->getBlock();
            $module = $this->moduleManager->get($pageBlock->getModuleId());

            if ($area === null || $module === null) {
                continue;
            }

            $loadedBlocks->set($area->getId(), new LoadedBlock(
                $area, $module, $block->getActionId(), $pageBlock->getTemplateId(),
                $block->getTitle(), $block->getParameters(), $this->debug
            ));
        }

        $loadedBlocks = $loadedBlocks->matching(Criteria::create()->orderBy(['main' => Criteria::DESC]));

        return new LoadedPage($page, $layout, $loadedBlocks, $this->debug);
    }

    /**
     * @param string $moduleId
     * @param string $locale
     * @return Page
     * @throws NonUniqueResultException
     */
    public function getModuleBasePage($moduleId, $locale)
    {
        $expr = new Expr();

        /** @var Layout[] $layoutsWithMainArea */
        $layoutsWithMainArea = array_filter(
            $this->layoutManager->getAll(),
            function(Layout $layout) {
                return $layout->getMainArea() !== null;
            }
        );

        $pageAreaOrExpr = $expr->orX();

        foreach ($layoutsWithMainArea as $layout) {
            $pageAreaOrExpr->add($expr->andX()
                ->add($expr->eq('p.layoutId', $expr->literal($layout->getId())))
                ->add($expr->eq('pb.areaId', $expr->literal($layout->getMainArea()->getId())))
            );
        }

        /** @var QueryBuilder $queryBuilder */
        $queryBuilder = $this->entityManager
            ->getRepository(Page::class)
            ->createQueryBuilder('p')
            ->leftJoin('p.blocks', 'pb')
            ->leftJoin('pb.block', 'b')
            ->andWhere($expr->eq('p.locale', $expr->literal($locale)))
            ->andWhere($expr->eq('b.moduleId', $expr->literal($moduleId)))
            ->andWhere($pageAreaOrExpr)
            ->orderBy('p.sortOrder')
            ->setMaxResults(1)
        ;

        /** @var Page $page */
        $page = $queryBuilder->getQuery()->getOneOrNullResult();

        return $page;
    }

    /**
     * @param string $moduleId
     * @param string $actionId
     * @param string $locale
     * @return Page
     * @throws NonUniqueResultException
     */
    public function getModuleActionPage($moduleId, $actionId, $locale)
    {
        $expr = new Expr();

        /** @var Layout[] $layoutsWithMainArea */
        $layoutsWithMainArea = array_filter(
            $this->layoutManager->getAll(),
            function(Layout $layout) {
                return $layout->getMainArea() !== null;
            }
        );

        $pageAreaOrExpr = $expr->orX();

        foreach ($layoutsWithMainArea as $layout) {
            $pageAreaOrExpr->add($expr->andX()
                ->add($expr->eq('p.layoutId', $expr->literal($layout->getId())))
                ->add($expr->eq('pb.areaId', $expr->literal($layout->getMainArea()->getId())))
            );
        }

        /** @var QueryBuilder $queryBuilder */
        $queryBuilder = $this->entityManager
            ->getRepository(Page::class)
            ->createQueryBuilder('p')
            ->leftJoin('p.blocks', 'pb')
            ->leftJoin('pb.block', 'b')
            ->andWhere($expr->eq('p.locale', $expr->literal($locale)))
            ->andWhere($expr->eq('b.moduleId', $expr->literal($moduleId)))
            ->andWhere($expr->eq('b.actionId', $expr->literal($actionId)))
            ->andWhere($pageAreaOrExpr)
            ->orderBy('p.sortOrder')
            ->setMaxResults(1)
        ;

        /** @var Page $pages */
        $page = $queryBuilder->getQuery()->getOneOrNullResult();

        return $page;
    }
}
