<?php

namespace Megacoders\PageBundle;

use Megacoders\PageBundle\DependencyInjection\Compiler\LayoutPass;
use Megacoders\PageBundle\DependencyInjection\Compiler\ModulePass;
use Symfony\Component\DependencyInjection\ContainerBuilder;
use Symfony\Component\HttpKernel\Bundle\Bundle;

class MegacodersPageBundle extends Bundle
{
    /**
     * {@inheritdoc}
     */
    public function build(ContainerBuilder $container)
    {
        $container
            ->addCompilerPass(new LayoutPass())
            ->addCompilerPass(new ModulePass())
        ;
    }
}
