<?php

namespace Megacoders\PageBundle\Form\DataTransformer;

use Megacoders\PageBundle\Manager\ModuleManager;
use Symfony\Component\Form\DataTransformerInterface;

class StringToModuleTransformer implements DataTransformerInterface
{
    /**
     * @var ModuleManager
     */
    private $moduleManager;

    /**
     * StringToModuleTransformer constructor.
     * @param ModuleManager $moduleManager
     */
    public function __construct(ModuleManager $moduleManager)
    {
        $this->moduleManager = $moduleManager;
    }

    /**
     * {@inheritdoc}
     */
    public function transform($moduleId)
    {
        return $this->moduleManager->get($moduleId);
    }

    /**
     * {@inheritdoc}
     */
    public function reverseTransform($module)
    {
        return $module === null ? null : $module->getId();
    }
}
