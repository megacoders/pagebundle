<?php

namespace Megacoders\PageBundle\Form\DataTransformer;

use Megacoders\PageBundle\Model\Layout;
use Symfony\Component\Form\DataTransformerInterface;

class StringToLayoutAreaTransformer implements DataTransformerInterface
{
    /**
     * @var Layout
     */
    private $layout;

    /**
     * StringToLayoutAreaTransformer constructor.
     * @param Layout $layout
     */
    public function __construct(Layout $layout)
    {
        $this->layout = $layout;
    }

    /**
     * {@inheritdoc}
     */
    public function transform($areaId)
    {
        return $this->layout->getArea($areaId);
    }

    /**
     * {@inheritdoc}
     */
    public function reverseTransform($area)
    {
        return $area === null ? null : $area->getId();
    }
}
