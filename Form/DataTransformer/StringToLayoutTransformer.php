<?php

namespace Megacoders\PageBundle\Form\DataTransformer;

use Megacoders\PageBundle\Manager\LayoutManager;
use Symfony\Component\Form\DataTransformerInterface;

class StringToLayoutTransformer implements DataTransformerInterface
{
    /**
     * @var LayoutManager
     */
    private $layoutManager;

    /**
     * StringToLayoutTransformer constructor.
     * @param LayoutManager $layoutManager
     */
    public function __construct(LayoutManager $layoutManager)
    {
        $this->layoutManager = $layoutManager;
    }

    /**
     * {@inheritdoc}
     */
    public function transform($layoutId)
    {
        return $this->layoutManager->get($layoutId);
    }

    /**
     * {@inheritdoc}
     */
    public function reverseTransform($layout)
    {
        return $layout === null ? null : $layout->getId();
    }
}
