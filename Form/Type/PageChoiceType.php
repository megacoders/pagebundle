<?php

namespace Megacoders\PageBundle\Form\Type;

use Megacoders\PageBundle\Form\ChoiceList\PageChoiceLoader;
use Megacoders\PageBundle\Manager\PageManager;
use Symfony\Bridge\Doctrine\Form\DataTransformer\CollectionToArrayTransformer;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\ChoiceList\Loader\ChoiceLoaderInterface;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\Options;
use Symfony\Component\OptionsResolver\OptionsResolver;

class PageChoiceType extends AbstractType
{
    /**
     * @var PageManager
     */
    private $pageManager;

    /**
     * @var ChoiceLoaderInterface[]
     */
    private $choiceLoaders = [];

    /**
     * PageTreeChoiceType constructor.
     * @param PageManager $pageManager
     */
    public function __construct(PageManager $pageManager)
    {
        $this->pageManager = $pageManager;
    }

    /**
     * {@inheritdoc}
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        if ($options['multiple']) {
            $builder->addViewTransformer(new CollectionToArrayTransformer(), true);
        }
    }

    /**
     * {@inheritdoc}
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $choiceLoader = function(Options $options) {
            if (null === $options['choices']) {
                $hash = serialize($options['site_id']);

                if (isset($this->choiceLoaders[$hash])) {
                    return $this->choiceLoaders[$hash];
                }

                $this->choiceLoaders[$hash] = new PageChoiceLoader($this->pageManager, $options['site_id']);

                return $this->choiceLoaders[$hash];
            }

            return null;
        };

        $resolver->setDefaults([
            'choices' => null,
            'choice_loader' => $choiceLoader,
            'choice_label' => 'treeName',
            'choice_value' => 'id',
            'site_id' => null
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function getParent()
    {
        return ChoiceType::class;
    }
}
