<?php

namespace Megacoders\PageBundle\Form\ChoiceList;

use Megacoders\PageBundle\Manager\PageManager;
use Symfony\Component\Form\ChoiceList\ArrayChoiceList;
use Symfony\Component\Form\ChoiceList\ChoiceListInterface;
use Symfony\Component\Form\ChoiceList\Loader\ChoiceLoaderInterface;

class PageChoiceLoader implements ChoiceLoaderInterface
{
    /**
     * @var ChoiceListInterface
     */
    private $choiceList;

    /**
     * @var PageManager
     */
    private $pageManager;

    /**
     * @var int|null
     */
    private $siteId;

    /**
     * PageChoiceLoader constructor.
     * @param PageManager $pageManager
     */
    public function __construct(PageManager $pageManager, $siteId = null)
    {
        $this->pageManager = $pageManager;
        $this->siteId = $siteId;
    }

    /**
     * {@inheritdoc}
     */
    public function loadChoiceList($value = null)
    {
        if ($this->choiceList) {
            return $this->choiceList;
        }

        $pages = $this->pageManager->getArray($this->siteId);

        return $this->choiceList = new ArrayChoiceList($pages, $value);
    }

    /**
     * {@inheritdoc}
     */
    public function loadChoicesForValues(array $values, $value = null)
    {
        if (empty($values)) {
            return [];
        }

        return $this->loadChoiceList($value)->getChoicesForValues($values);
    }

    /**
     * {@inheritdoc}
     */
    public function loadValuesForChoices(array $choices, $value = null)
    {
        if (empty($choices)) {
            return [];
        }

        return $this->loadChoiceList($value)->getValuesForChoices($choices);
    }
}
