<?php

namespace Megacoders\PageBundle\Http;

use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Exception\HttpException;

class ErrorResponse extends Response
{

    /**
     * @var \Throwable
     */
    private $exception;

    /**
     * ErrorResponse constructor.
     * @param \Throwable $exception
     */
    public function __construct(\Throwable $exception)
    {
        $this->exception = $exception;

        $statusCode = self::HTTP_INTERNAL_SERVER_ERROR;

        if ($exception instanceof HttpException) {
            $statusCode = $exception->getStatusCode();
        }

        parent::__construct($exception->getMessage(), $statusCode, []);
    }

    /**
     * @return \Throwable
     */
    public function getException()
    {
        return $this->exception;
    }

}
