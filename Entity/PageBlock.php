<?php

namespace Megacoders\PageBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;

/**
 * @ORM\Entity
 * @ORM\Table(name="page_block")
 * @UniqueEntity(
 *     fields={"page", "areaId"},
 *     errorPath="areaId",
 *     ignoreNull=false
 * )
 */
class PageBlock
{

    /**
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     * @var int
     */
    private $id;

    /**
     * @ORM\ManyToOne(targetEntity="Page", inversedBy="blocks")
     * @Assert\NotBlank()
     * @var Page
     */
    private $page;

    /**
     * @ORM\Column(type="string")
     * @Assert\NotBlank()
     * @var string
     */
    private $areaId;

    /**
     * @ORM\ManyToOne(targetEntity="Block", inversedBy="pageBlocks")
     * @Assert\NotBlank()
     * @var Block
     */
    private $block;

    /**
     * @ORM\Column(type="string")
     * @Assert\NotBlank()
     * @var string
     */
    private $templateId;

    /**
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param int $id
     * @return PageBlock
     */
    public function setId($id)
    {
        $this->id = $id;
        return $this;
    }

    /**
     * @return Page
     */
    public function getPage()
    {
        return $this->page;
    }

    /**
     * @param Page $page
     * @return PageBlock
     */
    public function setPage($page)
    {
        $this->page = $page;
        return $this;
    }

    /**
     * @return string
     */
    public function getAreaId()
    {
        return $this->areaId;
    }

    /**
     * @param string $areaId
     * @return PageBlock
     */
    public function setAreaId($areaId)
    {
        $this->areaId = $areaId;
        return $this;
    }

    /**
     * @return Block
     */
    public function getBlock()
    {
        return $this->block;
    }

    /**
     * @param Block $block
     * @return PageBlock
     */
    public function setBlock($block)
    {
        $this->block = $block;
        return $this;
    }

    /**
     * @return string
     */
    public function getTemplateId()
    {
        return $this->templateId;
    }

    /**
     * @param string $templateId
     * @return PageBlock
     */
    public function setTemplateId($templateId)
    {
        $this->templateId = $templateId;
        return $this;
    }

    /**
     * @return string|null
     */
    public function getModuleId()
    {
        $block = $this->getBlock();
        return $block ? $block->getModuleId() : null;
    }

    /**
     * @return string
     */
    public function getActionId()
    {
        $block = $this->getBlock();
        return $block ? $block->getActionId() : null;
    }

    /**
     * @return string
     */
    public function __toString()
    {
        return implode(' » ', [$this->getPage(), $this->getBlock()]);
    }

    public function __clone()
    {
        $this->setId(null);
    }
}
