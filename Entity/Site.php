<?php

namespace Megacoders\PageBundle\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;
use Megacoders\AdminBundle\Entity\ListEntityInterface;
use Symfony\Component\Validator\Constraints as Assert;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;

/**
 * @ORM\Entity
 * @ORM\Table(name="site")
 * @UniqueEntity("host")
 */
class Site implements ListEntityInterface
{
    const ADMIN_SESSION_PARAMETER_NAME = '_admin_site_id';

    /**
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     * @var int
     */
    private $id;

    /**
     * @ORM\Column(type="string")
     * @var string
     */
    private $name;

    /**
     * @ORM\Column(type="string")
     * @Assert\Regex("#^http[s]?\:\/\/(([a-zA-Z0-9]|[a-zA-Z0-9][a-zA-Z0-9\-]*[a-zA-Z0-9])\.)*([A-Za-z0-9]|[A-Za-z0-9][A-Za-z0-9\-]*[A-Za-z0-9])(\:\d+)?$#")
     * @var string
     */
    private $host;

    /**
     * @ORM\OneToOne(targetEntity="Page")
     * @var Page
     */
    private $mainPage;

    /**
     * @ORM\Column(type="boolean")
     * @var boolean
     */
    private $main;

    /**
     * @ORM\OneToMany(targetEntity="Page", mappedBy="site", orphanRemoval=true)
     * @ORM\OrderBy({"parent"="ASC", "sortOrder"="ASC"})
     * @var ArrayCollection
     */
    private $pages;

    /**
     * Site constructor.
     */
    public function __construct()
    {
        $this->main = false;
        $this->pages = new ArrayCollection();
    }

    /**
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param int $id
     * @return Site
     */
    public function setId($id)
    {
        $this->id = $id;
        return $this;
    }

    /**
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @param string $name
     * @return Site
     */
    public function setName($name)
    {
        $this->name = $name;
        return $this;
    }

    /**
     * @return string
     */
    public function getHost()
    {
        return $this->host;
    }

    /**
     * @param string $host
     * @return Site
     */
    public function setHost($host)
    {
        $this->host = $host;
        return $this;
    }

    /**
     * @return Page
     */
    public function getMainPage()
    {
        return $this->mainPage;
    }

    /**
     * @param Page $mainPage
     * @return Site
     */
    public function setMainPage($mainPage)
    {
        $this->mainPage = $mainPage;
        return $this;
    }

    /**
     * @return boolean
     */
    public function isMain()
    {
        return $this->main;
    }

    /**
     * @param boolean $main
     * @return Site
     */
    public function setMain($main)
    {
        $this->main = $main;
        return $this;
    }

    /**
     * @return ArrayCollection
     */
    public function getPages()
    {
        return $this->pages;
    }

    /**
     * @param ArrayCollection $pages
     * @return Site
     */
    public function setPages($pages)
    {
        $this->pages->clear();

        foreach ($pages as $page) {
            $this->addPage($page);
        }

        return $this;
    }

    /**
     * @param Page $page
     * @return Site
     */
    public function addPage(Page $page)
    {
        if (!$this->pages->contains($page)) {
            $this->pages->add($page);
        }

        return $this;
    }

    /**
     * @param string|null $part
     * @return mixed|null
     */
    public function getHostInfo($part = null)
    {
        $hostInfo = parse_url($this->getHost());

        return ($part === null) ? $hostInfo : (isset($hostInfo[$part]) ? $hostInfo[$part] : null);
    }

    /**
     * @return string
     */
    public function getRouteName()
    {
        return '__site__' .str_replace('.', '_', $this->getHostInfo('host'));
    }

    /**
     * @return string
     */
    public function __toString()
    {
        return (string) $this->getName();
    }
}
