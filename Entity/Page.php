<?php

namespace Megacoders\PageBundle\Entity;

use Megacoders\AdminBundle\Entity\ListEntityInterface;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;

/**
 * @ORM\Entity
 * @ORM\Table(name="page")
 * @UniqueEntity(
 *     fields={"site", "parent", "slug"},
 *     errorPath="slug",
 *     ignoreNull=false
 * )
 */
class Page implements ListEntityInterface
{

    const PAGE_TYPE_PAGE = 'PAGE';

    const PAGE_TYPE_LINK = 'LINK';

    const PAGE_TYPES_NAMES = [
        self::PAGE_TYPE_PAGE => 'admin.entities.page.type_page',
        self::PAGE_TYPE_LINK => 'admin.entities.page.type_link'
    ];

    const LINK_TYPE_FIRST_CHILD = 'FIRST_CHILD';

    const LINK_TYPE_PAGE = 'PAGE';

    const LINK_TYPE_URL = 'URL';

    const LINK_TYPES_NAMES = [
        self::LINK_TYPE_FIRST_CHILD => 'admin.entities.page.link_type_first_child',
        self::LINK_TYPE_PAGE => 'admin.entities.page.link_type_page',
        self::LINK_TYPE_URL => 'admin.entities.page.link_type_url'
    ];

    /**
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     * @var int
     */
    private $id;

    /**
     * @ORM\ManyToOne(targetEntity="Site", inversedBy="pages")
     * @Assert\NotBlank()
     * @var Site
     */
    private $site;

    /**
     * @ORM\ManyToOne(targetEntity="Page", inversedBy="children")
     * @var Page
     */
    private $parent;

    /**
     * @ORM\OneToMany(targetEntity="Page", mappedBy="parent", orphanRemoval=true)
     * @ORM\OrderBy({"sortOrder"="ASC"})
     * @var ArrayCollection
     */
    private $children;

    /**
     * @ORM\Column(type="string")
     * @Assert\NotBlank()
     * @Assert\Regex("#^[a-z0-9-]+$#")
     * @var string
     */
    private $slug;

    /**
     * @ORM\Column(type="text", nullable=true)
     * @var string
     */
    private $url;

    /**
     * @ORM\Column(type="string")
     * @Assert\NotBlank()
     * @var string
     */
    private $name;

    /**
     * @ORM\Column(type="integer")
     * @Assert\NotBlank()
     * @var int
     */
    private $sortOrder;

    /**
     * @ORM\Column(type="string")
     * @var string
     */
    private $type = self::PAGE_TYPE_PAGE;

    /**
     * @ORM\Column(type="string")
     * @Assert\NotBlank()
     * @var string
     */
    private $layoutId;

    /**
     * @ORM\Column(type="string", nullable = true)
     * @var string
     */
    private $linkType;

    /**
     * @ORM\ManyToOne(targetEntity="Page")
     * @ORM\JoinColumn(onDelete="SET NULL")
     * @var Page
     */
    private $linkPage;

    /**
     * @ORM\Column(type="string", length=2000, nullable=true)
     * @var string
     */
    private $linkUrl;

    /**
     * @ORM\OneToMany(targetEntity="PageBlock", mappedBy="page", cascade={"persist"}, orphanRemoval=true)
     * @var ArrayCollection|PageBlock[]
     */
    private $blocks;

    /**
     * @ORM\OneToMany(targetEntity="Meta", mappedBy="page", cascade={"persist"}, orphanRemoval=true)
     * @ORM\OrderBy({"name" = "ASC"})
     * @var ArrayCollection|Meta[]
     */
    private $metas;

    /**
     * @ORM\Column(type="string")
     * @Assert\NotBlank()
     * @var string
     */
    private $locale;

    /**
     * @ORM\Column(type="boolean")
     * @var boolean
     */
    private $published;

    /**
     * @ORM\Column(type="datetime")
     * @var \DateTime
     */
    private $createdAt;

    /**
     * @ORM\Column(type="datetime")
     * @var \DateTime
     */
    private $updatedAt;

    /**
     * @ORM\Column(type="datetime", nullable=true)
     * @var \DateTime
     */
    private $publishedAt;

    /**
     * Page constructor.
     */
    public function __construct()
    {
        $this->children = new ArrayCollection();
        $this->blocks = new ArrayCollection();
        $this->metas = new ArrayCollection();

        $now = new \DateTime();
        $this->createdAt = $now;
        $this->updatedAt = $now;
    }

    /**
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param int $id
     * @return Page
     */
    public function setId($id)
    {
        $this->id = $id;
        return $this;
    }

    /**
     * @return Site
     */
    public function getSite()
    {
        return $this->site;
    }

    /**
     * @param Site $site
     * @return Page
     */
    public function setSite($site)
    {
        $this->site = $site;
        return $this;
    }

    /**
     * @return Page
     */
    public function getParent()
    {
        return $this->parent;
    }

    /**
     * @param Page $parent
     * @return Page
     */
    public function setParent($parent)
    {
        $this->parent = $parent;
        return $this;
    }

    /**
     * @return Page|null
     */
    public function getFirstChild()
    {
        return $this->children->first();
    }

    /**
     * @return ArrayCollection|Page[]
     */
    public function getChildren()
    {
        return $this->children;
    }

    /**
     * @param ArrayCollection|Page[] $children
     * @return Page
     */
    public function setChildren($children)
    {
        $this->children->clear();

        foreach ($children as $page) {
            $this->addChildren($page);
        }

        return $this;
    }

    /**
     * @param Page $page
     * @return $this
     */
    public function addChildren(Page $page)
    {
        if (!$this->children->contains($page)) {
            $this->children->add($page);
        }

        return $this;
    }

    /**
     * @return string
     */
    public function getSlug()
    {
        return $this->slug;
    }

    /**
     * @param string $slug
     * @return Page
     */
    public function setSlug($slug)
    {
        $this->slug = $slug;
        return $this;
    }

    /**
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @param string $name
     * @return Page
     */
    public function setName($name)
    {
        $this->name = $name;
        return $this;
    }

    /**
     * @return int
     */
    public function getSortOrder()
    {
        return $this->sortOrder;
    }

    /**
     * @param int $sortOrder
     * @return Page
     */
    public function setSortOrder($sortOrder)
    {
        $this->sortOrder = $sortOrder;
        return $this;
    }

    /**
     * @return string
     */
    public function getType()
    {
        return $this->type;
    }

    /**
     * @return string|null
     */
    public function getPageTypeName()
    {
        $types = self::PAGE_TYPES_NAMES;

        if (isset($types[$this->getType()])) {
            return $types[$this->getType()];
        }

        return null;
    }

    /**
     * @param string $type
     * @return Page
     */
    public function setType($type)
    {
        $this->type = $type;
        return $this;
    }

    /**
     * @return string
     */
    public function getLayoutId()
    {
        return $this->layoutId;
    }

    /**
     * @param string $layoutId
     * @return Page
     */
    public function setLayoutId($layoutId)
    {
        $this->layoutId = $layoutId;
        return $this;
    }

    /**
     * @return string
     */
    public function getLinkType()
    {
        return $this->linkType;
    }

    /**
     * @return string|null
     */
    public function getLinkTypeName()
    {
        $types = self::LINK_TYPES_NAMES;

        if (isset($types[$this->getLinkType()])) {
            return $types[$this->getLinkType()];
        }

        return null;
    }

    /**
     * @param string $linkType
     * @return Page
     */
    public function setLinkType($linkType)
    {
        $this->linkType = $linkType;
        return $this;
    }

    /**
     * @return Page
     */
    public function getLinkPage()
    {
        return $this->linkPage;
    }

    /**
     * @param Page $linkPage
     * @return Page
     */
    public function setLinkPage($linkPage)
    {
        $this->linkPage = $linkPage;
        return $this;
    }

    /**
     * @return string
     */
    public function getLinkUrl()
    {
        return $this->linkUrl;
    }

    /**
     * @param string $linkUrl
     * @return Page
     */
    public function setLinkUrl($linkUrl)
    {
        $this->linkUrl = $linkUrl;
        return $this;
    }

    /**
     * @return ArrayCollection|PageBlock[]
     */
    public function getBlocks()
    {
        return $this->blocks;
    }

    /**
     * @param string $areaId
     * @return PageBlock|null
     */
    public function getBlock($areaId) {
        foreach ($this->blocks as $block) {
            if ($block->getAreaId() == $areaId) {
                return $block;
            }
        }

        return null;
    }

    /**
     * @param ArrayCollection|PageBlock[] $blocks
     * @return Page
     */
    public function setBlocks($blocks)
    {
        $this->blocks->clear();

        foreach ($blocks as $block) {
            $this->addBlock($block);
        }

        return $this;
    }

    /**
     * @param PageBlock $block
     * @return Page
     */
    public function addBlock(PageBlock $block)
    {
        if (!$this->blocks->contains($block)) {
            $this->blocks->add($block);
            $block->setPage($this);
        }

        return $this;
    }

    /**
     * @return ArrayCollection|Meta[]
     */
    public function getMetas()
    {
        return $this->metas;
    }

    /**
     * @param string $name
     * @return Meta|null
     */
    public function getMeta($name) {
        foreach ($this->metas as $meta) {
            if ($meta->getName() == $name) {
                return $meta;
            }
        }

        return null;
    }

    /**
     * @param ArrayCollection|Meta[] $metas
     * @return Page
     */
    public function setMetas($metas)
    {
        $this->metas->clear();

        foreach ($metas as $meta) {
            $this->addMeta($meta);
        }

        return $this;
    }

    /**
     * @param Meta $meta
     * @return Page
     */
    public function addMeta(Meta $meta)
    {
        if (!$this->metas->containsKey($meta->getName())) {
            $this->setMeta($meta);
            $meta->setPage($this);
        }

        return $this;
    }

    public function setMeta(Meta $meta) {
        $this->metas->set($meta->getName(), $meta);
    }

    /**
     * @return string
     */
    public function getLocale()
    {
        return $this->locale;
    }

    /**
     * @param string $locale
     * @return Page
     */
    public function setLocale($locale)
    {
        $this->locale = $locale;
        return $this;
    }

    /**
     * @return bool
     */
    public function isPublished()
    {
        return $this->published;
    }

    /**
     * @param bool $published
     * @return Page
     */
    public function setPublished($published)
    {
        $this->published = $published;
        return $this;
    }

    /**
     * @return \DateTime
     */
    public function getCreatedAt()
    {
        return $this->createdAt;
    }

    /**
     * @param \DateTime $createdAt
     * @return Page
     */
    public function setCreatedAt($createdAt)
    {
        $this->createdAt = $createdAt;
        return $this;
    }

    /**
     * @return \DateTime
     */
    public function getUpdatedAt()
    {
        return $this->updatedAt;
    }

    /**
     * @param \DateTime $updatedAt
     * @return Page
     */
    public function setUpdatedAt($updatedAt)
    {
        $this->updatedAt = $updatedAt;
        return $this;
    }

    /**
     * @return \DateTime
     */
    public function getPublishedAt()
    {
        return $this->publishedAt;
    }

    /**
     * @param \DateTime $publishedAt
     * @return Page
     */
    public function setPublishedAt($publishedAt)
    {
        $this->publishedAt = $publishedAt;
        return $this;
    }

    /**
     * @return int
     */
    public function getLevel()
    {
        $level = 0;
        $parent = $this;

        while ($parent = $parent->getParent()) {
            $level += 1;
        }

        return $level;
    }

    /**
     * @param string $padding
     * @return string
     */
    public function getTreeName($padding = '— ')
    {
        return str_repeat($padding, $this->getLevel()) .$this->getName();
    }

    /**
     * @return string
     */
    public function getPath()
    {
        $path = '/' .$this->getSlug();
        $parent = $this->getParent();

        if ($parent === null) {
            return $path;

        } else {
            return $parent->getPath() .$path;
        }
    }

    /**
     * @param bool $absolute
     * @return string
     */
    public function getTargetUrl($absolute = false) {
        if ($this->type == self::PAGE_TYPE_LINK) {
            switch ($this->linkType) {
                case self::LINK_TYPE_FIRST_CHILD:
                    $firstChild = $this->getFirstChild();
                    return $firstChild ? $firstChild->getUrl($absolute) : null;

                case $this->linkType == self::LINK_TYPE_PAGE:
                    return $this->linkPage ? $this->linkPage->getUrl($absolute) : null;

                default:
                    return $this->linkUrl;
            }
        }

        return $this->getUrl();
    }

    /**
     * @param bool $absolute
     * @return string
     */
    public function getUrl($absolute = false)
    {
        $path = $this->url ?: $this->getPath();
        return ($absolute ? $this->getSite()->getHost() : '') .$path;
    }

    /**
     * @param string $url
     * @return Page
     */
    public function setUrl($url)
    {
        $this->url = $url;
        return $this;
    }

    /**
     * @return string
     */
    public function getRouteName()
    {
        return $this->getSite()->getRouteName() .'__page_' .str_replace('/', '_', $this->getUrl());
    }

    /**
     * @return string
     */
    public function __toString()
    {
        return (string) $this->getName();
    }

    public function __clone()
    {
        $now = (new \DateTime())->format('ymdhis');

        $this
            ->setId(null)
            ->setName($this->getName() .' (Clone ' .$now .')')
            ->setSlug($this->getSlug() .'-clone-' .$now)
        ;

        /** Clone blocks */
        $blocks = $this->getBlocks();
        $this->blocks = new ArrayCollection();

        foreach ($blocks as $block) {
            $blockClone = clone $block;
            $this->addBlock($blockClone);
        }

        /** Clone meta */
        $metas = $this->getMetas();
        $this->metas = new ArrayCollection();

        foreach ($metas as $meta) {
            $metaClone = clone $meta;
            $this->addMeta($metaClone);
        }
    }

}
