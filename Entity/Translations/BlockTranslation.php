<?php

namespace Megacoders\PageBundle\Entity\Translations;

use Doctrine\ORM\Mapping as ORM;
use Megacoders\PageBundle\Entity\Block;
use Sonata\TranslationBundle\Model\Gedmo\AbstractPersonalTranslation;

/**
 * @ORM\Entity
 * @ORM\Table(
 *     name="block_translation",
 *     uniqueConstraints={@ORM\UniqueConstraint(name="lookup_unique_block_translation_idx", columns={
 *         "locale", "object_id", "field"
 *     })}
 * )
 */
class BlockTranslation extends AbstractPersonalTranslation
{
    /**
     * @ORM\ManyToOne(targetEntity="Megacoders\PageBundle\Entity\Block", inversedBy="translations")
     * @ORM\JoinColumn(name="object_id", referencedColumnName="id", onDelete="CASCADE")
     * @var Block
     */
    protected $object;

}
