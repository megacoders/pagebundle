<?php

namespace Megacoders\PageBundle\Routing;

use Megacoders\PageBundle\Controller\Module\RoutesConfigurableInterface;
use Megacoders\PageBundle\Manager\ModuleManager;
use Megacoders\PageBundle\Manager\PageManager;
use Megacoders\PageBundle\Manager\SiteManager;
use Symfony\Component\Config\Loader\Loader;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\Routing\Route;
use Symfony\Component\Routing\RouteCollection;

class PageRouteLoader extends Loader
{
    /**
     * @var SiteManager
     */
    private $siteManager;

    /**
     * @var PageManager
     */
    private $pageManager;

    /**
     * @var ModuleManager
     */
    private $moduleManager;

    /**
     * PageRouteLoader constructor.
     * @param ContainerInterface $container
     */
    public function __construct(ContainerInterface $container)
    {
        $this->siteManager = $container->get('page.manager.site_manager');
        $this->pageManager = $container->get('page.manager.page_manager');
        $this->moduleManager = $container->get('page.manager.module_manager');
    }

    /**
     * {@inheritdoc}
     */
    public function load($resource, $type = null)
    {
        $collection = new RouteCollection();

        $this->loadPageRoutes($collection);
        $this->loadSystemServiceRoutes($collection);
        $this->loadModuleServiceRoutes($collection);

        return $collection;
    }

    /**
     * {@inheritdoc}
     */
    public function supports($resource, $type = null)
    {
        return $type === 'page';
    }

    /**
     * @param RouteCollection $collection
     */
    private function loadPageRoutes(RouteCollection $collection)
    {
        $sites = $this->siteManager->loadAll();

        foreach ($sites as $site) {
            $collection->addCollection($this->siteManager->configureRoutes($site));
        }
    }

    /**
     * @param RouteCollection $collection
     */
    private function loadSystemServiceRoutes(RouteCollection $collection)
    {
        $collection->add(
            '_system_ajax_block',
            new Route(
                '_system/ajax/block/{_pageId}/{_areaId}',
                ['_controller' => 'MegacodersPageBundle:Page:block'],
                [
                    '_pageId' => '\d+',
                    '_areaId' => '[0-9a-z_]+'
                ]
            )
        );
    }

    /**
     * @param RouteCollection $collection
     */
    private function loadModuleServiceRoutes(RouteCollection $collection)
    {
        $modules = $this->moduleManager->getAll();

        foreach ($modules as $module) {
            $controller = $module->getController();

            if ($controller instanceof RoutesConfigurableInterface) {
                $moduleServiceRoutes = $controller->configureServiceRoutes();

                if ($moduleServiceRoutes instanceof RouteCollection) {
                    $collection->addCollection($moduleServiceRoutes);
                }
            }
        }
    }
}
