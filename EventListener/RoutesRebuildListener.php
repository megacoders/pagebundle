<?php

namespace Megacoders\PageBundle\EventListener;

use Megacoders\PageBundle\Entity\Block;
use Megacoders\PageBundle\Entity\Page;
use Megacoders\PageBundle\Entity\PageBlock;
use Megacoders\PageBundle\Entity\Site;
use Doctrine\Common\Persistence\Event\LifecycleEventArgs;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\Filesystem\Filesystem;
use Symfony\Component\Finder\Finder;

class RoutesRebuildListener
{
    /**
     * @var ContainerInterface
     */
    private $container;

    /**
     * @var array
     */
    private $allowedClasses = [
        Site::class,
        Page::class,
        PageBlock::class,
        Block::class
    ];

    /**
     * RoutesRebuildSubscriber constructor.
     * @param ContainerInterface $container
     */
    public function __construct(ContainerInterface $container)
    {
        $this->container = $container;
    }

    /**
     * @param LifecycleEventArgs $args
     */
    public function postPersist(LifecycleEventArgs $args)
    {
        $object = $args->getObject();

        if ($this->checkEntityClass($object)) {
            $this->clearRoutingCache();
        }

        if ($object instanceof Page) {
            $this->updatePageUrl($object);
        }
    }

    /**
     * @param LifecycleEventArgs $args
     */
    public function postUpdate(LifecycleEventArgs $args)
    {
        $object = $args->getObject();

        if ($this->checkEntityClass($object)) {
            $this->clearRoutingCache();
        }

        if ($object instanceof Page) {
            $this->updatePageUrl($object);
        }
    }

    /**
     * @param LifecycleEventArgs $args
     */
    public function postRemove(LifecycleEventArgs $args)
    {
        if ($this->checkEntityClass($args->getObject())) {
            $this->clearRoutingCache();
        }
    }

    /**
     * @param object $entity
     * @return bool
     */
    private function checkEntityClass($entity)
    {
        foreach ($this->allowedClasses as $class) {
            if (is_a($entity, $class)) {
                return true;
            }
        }

        return false;
    }

    private function clearRoutingCache()
    {
        $finder = new Finder();
        $filesystem = new Filesystem();

        $cacheDir = $this->container->getParameter('kernel.cache_dir');
        $cacheClasses = [
            $this->container->getParameter('router.options.generator.cache_class'),
            $this->container->getParameter('router.options.matcher.cache_class')
        ];

        foreach ($cacheClasses as $cacheClass) {
            $files = $finder->in($cacheDir)->files()->name($cacheClass .'.php*');
            $filesystem->remove($files);
        }
    }

    /**
     * @param Page $page
     */
    private function updatePageUrl(Page $page)
    {
        $page->setUrl($page->getPath());
    }
}
